# Example Symfony Project
You must first shut down your containers, if any (`nginx`, `postgres`, `redis`) or stop the services, because ports are standard.

`make run` - run the project

`docker compose exec -it php-fpm bash` | `make bash` - get inside the container

### Commands inside the container

Use `make` or `make help` to view the list of commands you can use  

`./migrate.sh up` - up the migrations  
`./bin/console file:fixtures:load --purge-with-truncate` - load fixtures  
`./vendor/bin/phpstan analyse` - phpstan analyse  
`./vendor/bin/phpcs` - CodeSniffer find violations  
`./vendor/bin/phpcbf` - CodeSniffer fix violations  
`./bin/console doctrine:cache:clear-metadata --flush` - Doctrine cache clear


### TODO
- logout path
- refreshToken invalidation on logout
- fix /api | /other methods for users
- create a token extractor for applications token
