file := ./docker-compose.local.yml

default: help

.PHONY: help
help:
	@echo -e "Usage: \033[32mmake\033[0m \033[36mcommand\033[0m"
	@echo
	@echo -e "\e[32mCommands:\e[0m"
	@printf "\033[36m%-25s\033[0m \033[32m%s\033[0m\n" "run" "Run the application"
	$(call list)

.PHONY: run
run: copy-env copy-compose up vendor generate-keypair migrate fixtures

.PHONY: copy-env
copy-env: ## Create .env from .env.dev
	@if [ ! -e  .env ]; then cp .env.dev .env; fi

.PHONY: copy-compose
copy-compose: ## Create the docker-compose.local.yml
	@if [ ! -e  docker-compose.local.yml ]; then cp docker-compose.local.example.yml docker-compose.local.yml; fi

.PHONY: up
up: ## Create and start containers with the docker-compose.local.yml
	@docker compose --file $(file) up -d --build --force-recreate

.PHONY: vendor
vendor: ## Composer install within the php container
	@docker compose --file $(file) exec -i php-fpm composer install

.PHONY: generate-keypair
generate-keypair: ## Generate JWT pem keys (public and private) to ./config/jwt
	@docker compose --file $(file) exec -i php-fpm ./bin/console lexik:jwt:generate-keypair --skip-if-exists

.PHONY: migrate
migrate: ## Run migrate.sh up
	@./migrate.sh up

#.PHONY: migrate
#migrate: ## Run migrate.sh up
#	@docker compose --file $(file) exec -i php-fpm php ./bin/console d:m:m -q

.PHONY: fixtures
fixtures: ## Load fixtures through doctrine within the php container
	@docker compose --file $(file) exec -i php-fpm ./bin/console file:fixtures:load --purge-with-truncate -q

.PHONY: down
down: ## Stop and remove containers, networks with the docker-compose.local.yml
	@docker compose --file $(file) down

.PHONY: ps
ps: ## List containers with the docker-compose.yml
	@docker compose --file $(file) ps -a

.PHONY: stan
stan: ## Static analyses of the code
	@docker compose --file $(file) exec -i php-fpm ./vendor/bin/phpstan

.PHONY: fix
fix: ## Codesniffer fixer
	@docker compose --file $(file) exec -i php-fpm ./vendor/bin/phpcbf

.PHONY: lint
lint: ## Codesniffer lint
	@docker compose --file $(file) exec -i php-fpm ./vendor/bin/phpcs

.PHONY: cc
cc: ## Clear application and doctrine Cache
	@docker compose --file $(file) exec -i php-fpm php ./bin/console c:c
	@docker compose --file $(file) exec -i php-fpm php ./bin/console d:c:clear-metadata --flush
	@docker compose --file $(file) exec -i php-fpm php ./bin/console doctrine:cache:clear-query --flush
	@docker compose --file $(file) exec -i php-fpm php ./bin/console d:c:clear-result --flush

.PHONY: unit
unit: ## Run unit tests
	@docker compose --file $(file) exec -i php-fpm ./vendor/bin/phpunit

.PHONY: bash
bash: ## Get bash into the php-fpm container
	@docker compose --file $(file) exec -it php-fpm bash

define list
	@grep -E '^[a-zA-Z_-]+:.*?## .*' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-25s\033[0m \033[32m%s\033[0m\n", $$1, $$2}'
endef
