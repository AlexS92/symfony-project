<?php
declare(strict_types=1);

namespace App\Tests\unit;

use PHPUnit\Framework\TestCase;
use App\Controller\IndexController;

class IndexControllerTest extends TestCase
{
    /** @var string  */
    private const EXPECTED_REDIRECT_LOCATION = '/api/doc';
    /** @var int  */
    private const EXPECTED_REDIRECT_CODE = 302;
    /** @var string  */
    private const REDIRECT_HEADER = 'location';
    private IndexController $indexController;

    public function setUp(): void
    {
        $this->indexController = new IndexController();
    }

    /** @test */
    public function testIndexControllerRedirectCode(): void
    {
        $response = $this->indexController->index();
        $actualStatusCode = $response->getStatusCode();
        $this->assertEquals(self::EXPECTED_REDIRECT_CODE, $actualStatusCode);
    }

    /** @test */
    public function testIndexControllerRedirectLocation(): void
    {
        $response = $this->indexController->index();
        $actualRedirectLocation = $response->headers->get(self::REDIRECT_HEADER);
        $this->assertEquals(self::EXPECTED_REDIRECT_LOCATION, $actualRedirectLocation);
    }
}
