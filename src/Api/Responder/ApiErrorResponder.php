<?php

namespace App\Api\Responder;

use App\Api\Response\Error\ApiErrorResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

readonly class ApiErrorResponder
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param array<string, mixed> $serializerContext
     * @param array<string, mixed> $headers
     */
    public function createResponse(ApiErrorResponse $response, int $httpStatusCode = Response::HTTP_BAD_REQUEST, array $headers = [], array $serializerContext = []): JsonResponse
    {
        $response = $this->serializer->serialize($response, JsonEncoder::FORMAT, $serializerContext);

        return new JsonResponse($response, $httpStatusCode, $headers, true);
    }
}
