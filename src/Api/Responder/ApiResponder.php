<?php
declare(strict_types=1);

namespace App\Api\Responder;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

readonly class ApiResponder
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param array<string, mixed> $serializerContext
     * @param array<string, mixed> $headers
     */
    public function createResponse(mixed $response, int $httpStatusCode = Response::HTTP_OK, array $headers = [], array $serializerContext = []): JsonResponse
    {
        $needSerialization = (null !== $response);
        if ($needSerialization) {
            $response = $this->serializer->serialize($response, JsonEncoder::FORMAT, $serializerContext);
        }

        return new JsonResponse($response, $httpStatusCode, $headers, $needSerialization);
    }
}
