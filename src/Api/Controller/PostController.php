<?php
declare(strict_types=1);

namespace App\Api\Controller;

use App\Api\Enum\SerializationGroup;
use App\Api\Request\CreatePostDto;
use App\Api\Request\CreatePostDtoCollection;
use App\Api\Request\EditPostDto;
use App\Api\Responder\ApiResponder;
use App\Api\Response\Error\ApiErrorResponse;
use App\Api\Response\PostResponse;
use App\Entity\Application;
use App\Entity\Post;
use App\Enum\ApplicationRoleEnum;
use App\Exception\DomainException;
use App\Service\PostService;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

#[OA\Tag(name: "Post")]
class PostController extends AbstractController
{
    private readonly ApiResponder $responder;
    private readonly PostService $postService;

    public function __construct(ApiResponder $responder, PostService $postService)
    {
        $this->responder = $responder;
        $this->postService = $postService;
    }

    #[OA\Get(description: "Post list")]
    #[OA\Response(
        response: 200,
        description: "Post list",
        content: new OA\JsonContent(
            type: "array",
            items: new OA\Items(
                ref: new Model(type: PostResponse::class, groups: [SerializationGroup::LIST])
            )
        )
    )]
    #[OA\Response(
        response: 400,
        description: "Error",
        content: new OA\JsonContent(
            type: "array",
            items: new OA\Items(ref: new Model(type: ApiErrorResponse::class)),
            minItems: 1
        )
    )]
    #[Route(path: "posts", name: "get-post-list", methods: ["GET"])]
    #[IsGranted(ApplicationRoleEnum::ROLE_APPLICATION_READ->value)]
    public function getPostList(Application $application): JsonResponse
    {
        $posts = $this->postService->findPostsByApplication($application);

        $response = [];
        foreach ($posts as $post) {
            $response[] = new PostResponse($post);
        }

        return $this->responder->createResponse($response, serializerContext: [
            AbstractNormalizer::GROUPS => [SerializationGroup::LIST]
        ]);
    }

    #[OA\Get(description: "Post by slug")]
    #[OA\Response(
        response: 200,
        description: "Post",
        content: new OA\JsonContent(
            ref: new Model(type: PostResponse::class, groups: [SerializationGroup::LIST, SerializationGroup::DETAIL])
        )
    )]
    #[OA\Response(
        response: 404,
        description: "Not Found",
        content: new OA\JsonContent(
            type: "array",
            items: new OA\Items(ref: new Model(type: ApiErrorResponse::class)),
            minItems: 1
        )
    )]
    #[Route(path: "posts/{slug}", name: "get-post", methods: ["GET"])]
    #[IsGranted(ApplicationRoleEnum::ROLE_APPLICATION_READ->value)]
    public function getPost(Post $post): JsonResponse
    {
        return $this->responder->createResponse(new PostResponse($post), serializerContext: [
            AbstractNormalizer::GROUPS => [SerializationGroup::LIST, SerializationGroup::DETAIL]
        ]);
    }

    /** @throws DomainException */
    #[OA\Post(description: "Create Post")]
    #[OA\RequestBody(
        request: "CreatePostDto",
        required: true,
        content: new OA\JsonContent(ref: new Model(type: CreatePostDto::class))
    )]
    #[OA\Response(
        response: 200,
        description: "Post",
        content: new OA\JsonContent(
            ref: new Model(type: PostResponse::class, groups: [SerializationGroup::LIST, SerializationGroup::DETAIL])
        )
    )]
    #[OA\Response(
        response: 400,
        description: "Error",
        content: new OA\JsonContent(
            type: "array",
            items: new OA\Items(ref: new Model(type: ApiErrorResponse::class)),
            minItems: 1
        )
    )]
    #[Route(path: "posts", name: "create-post", methods: ["POST"])]
    #[IsGranted(ApplicationRoleEnum::ROLE_APPLICATION_WRITE->value)]
    public function createPost(Application $application, CreatePostDto $createPostDto): JsonResponse
    {
        $post = $this->postService->createPostByDto($application, $createPostDto);
        return $this->responder->createResponse(new PostResponse($post));
    }

    /** @throws DomainException */
    #[OA\Post(description: "Multi Create Post")]
    #[OA\RequestBody(
        request: "CreatePostDto",
        required: true,
        content: new OA\JsonContent(
            ref: new Model(type: CreatePostDto::class),
        )
    )]
    #[OA\Response(
        response: 200,
        description: "Post list",
        content: new OA\JsonContent(
            type: "array",
            items: new OA\Items(
                ref: new Model(type: PostResponse::class, groups: [SerializationGroup::LIST])
            )
        )
    )]
    #[OA\Response(
        response: 400,
        description: "Error",
        content: new OA\JsonContent(
            type: "array",
            items: new OA\Items(ref: new Model(type: ApiErrorResponse::class)),
            minItems: 1
        )
    )]
    #[Route(path: "posts/multi-create", name: "multi-create-post", methods: ["POST"])]
    #[IsGranted(ApplicationRoleEnum::ROLE_APPLICATION_WRITE->value)]
    public function multiCreatePost(Application $application, CreatePostDtoCollection $createPostDtoCollection): JsonResponse
    {
        $postList = $this->postService->multiCreatePost($createPostDtoCollection, $application);

        $response = [];
        foreach ($postList as $post) {
            $response[] = new PostResponse($post);
        }

        return $this->responder->createResponse($response, serializerContext: [
            AbstractNormalizer::GROUPS => [SerializationGroup::LIST]
        ]);
    }

    /** @throws DomainException */
    #[OA\Put(description: "Edit Post")]
    #[OA\RequestBody(
        request: "EditPostDto",
        required: true,
        content: new OA\JsonContent(ref: new Model(type: EditPostDto::class))
    )]
    #[OA\Response(
        response: 200,
        description: "Post",
        content: new OA\JsonContent(
            ref: new Model(
                type: PostResponse::class,
                groups: [SerializationGroup::LIST, SerializationGroup::DETAIL]
            )
        )
    )]
    #[OA\Response(
        response: 400,
        description: "Error",
        content: new OA\JsonContent(
            type: "array",
            items: new OA\Items(ref: new Model(type: ApiErrorResponse::class)),
            minItems: 1
        )
    )]
    #[Route(path: "posts/{slug}", name: "edit-post", methods: ["PUT"])]
    #[IsGranted(
        attribute: new Expression('0 === user.getApplication().getId().compare(subject)'),
        subject: new Expression('args["post"].getApplication().getId()')
    )]
    #[IsGranted(ApplicationRoleEnum::ROLE_APPLICATION_WRITE->value)]
    public function editPost(Post $post, EditPostDto $editPostDto): JsonResponse
    {
        $post = $this->postService->editPostByDto($post, $editPostDto);
        return $this->responder->createResponse(new PostResponse($post));
    }
}
