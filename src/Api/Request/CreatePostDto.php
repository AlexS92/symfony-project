<?php
declare(strict_types=1);

namespace App\Api\Request;

use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AppAssert;

#[OA\Schema(title: "Create Post", description: "Create Post", required: ["title"])]
#[Assert\GroupSequence(groups: ["CreatePostDto", "UniquePost"])]
readonly class CreatePostDto implements BodyRequestDtoMarker
{
    #[Assert\NotBlank]
    #[Assert\Length(max:255)]
    #[AppAssert\UniquePost(groups: ["UniquePost"])]
    #[OA\Property(title: "Post Title", description: "Post Title")]
    private string $title;

    /** @var string[] */
    #[Assert\Count(min: 1)]
    #[Assert\All([
        new Assert\NotBlank(),
        new Assert\Length(min: 3)
    ])]
    #[OA\Property(title: "Post Tags", description: "Post Tags")]
    private array $tags;

    /** @param string[] $tags */
    public function __construct(string $title, array $tags = [])
    {
        $this->title = $title;
        $this->tags = $tags;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /** @return string[] */
    public function getTags(): array
    {
        return $this->tags;
    }
}
