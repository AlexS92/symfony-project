<?php
declare(strict_types=1);

namespace App\Api\Request;

use App\Enum\ApplicationStatusEnum;
use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AppAssert;
use App\Entity\Application;

#[OA\Schema(title: "Create Application", description: "Create Application", required: ["name"])]
#[Assert\GroupSequence(groups: ["CreateApplicationDto", "UniqueApplication"])]
readonly class CreateApplicationDto implements BodyRequestDtoMarker
{
    #[Assert\NotBlank]
    #[AppAssert\EntityExist(entityClass: Application::class, field: "name", isExistenceRequired: false, groups: ["UniqueApplication"])]
    #[OA\Property(title: "Application name", description: "Application name")]
    private string $name;

    #[Assert\NotBlank(allowNull: true)]
    #[OA\Property(title: "Application description", description: "Application description", type: "string", nullable: true)]
    private ?string $description;

    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Type(type: ApplicationStatusEnum::class)]
    #[OA\Property(title: "Application status", description: "Application status", type: "string", enum: ApplicationStatusEnum::CASES, nullable: true)]
    private ApplicationStatusEnum $status;

    public function __construct(string $name, ?string $description = null, ?ApplicationStatusEnum $status = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->status = $status ?? ApplicationStatusEnum::from(ApplicationStatusEnum::DEFAULT);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getStatus(): ApplicationStatusEnum
    {
        return $this->status;
    }
}
