<?php
declare(strict_types=1);

namespace App\Api\Request;

use Ramsey\Collection\AbstractCollection;

/** @template CreatePostDto */
class CreatePostDtoCollection extends AbstractCollection
{
    #[\Override]
    public function getType(): string
    {
        return CreatePostDto::class;
    }
}
