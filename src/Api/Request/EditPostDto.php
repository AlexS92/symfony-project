<?php
declare(strict_types=1);

namespace App\Api\Request;

use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Constraints as Assert;

#[OA\Schema(title: "Edit Post", description: "Edit Post", required: ["tags"])]
readonly class EditPostDto implements BodyRequestDtoMarker
{
    /** @var string[] */
    #[Assert\Count(min: 1)]
    #[Assert\All([
        new Assert\NotBlank(),
        new Assert\Length(min: 3)
    ])]
    #[OA\Property(title: "Post Tags", description: "Post Tags", type: "array", items: new OA\Items(type: "string"))]
    private array $tags;

    /** @param string[] $tags */
    public function __construct(array $tags = [])
    {
        $this->tags = $tags;
    }

    /** @return string[] */
    public function getTags(): array
    {
        return $this->tags;
    }
}
