<?php
declare(strict_types=1);

namespace App\Api\Enum;

enum SerializationGroup
{
    public const LIST = 'list';
    public const DETAIL = 'detail';
    public const TOKEN = 'token';
}
