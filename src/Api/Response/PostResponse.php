<?php
declare(strict_types=1);

namespace App\Api\Response;

use App\Entity\Post;
use App\Enum\PostStatusEnum;
use OpenApi\Attributes as OA;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Api\Enum\SerializationGroup;

#[OA\Schema(title: "Post", description: "Post", required: ["slug", "title"])]
readonly class PostResponse
{
    #[Groups([SerializationGroup::LIST])]
    #[OA\Property(title: "Post title", description: "Post title")]
    private string $title;

    #[Groups([SerializationGroup::LIST])]
    #[OA\Property(title: "Post slug", description: "Post slug")]
    private string $slug;

    #[Groups([SerializationGroup::LIST])]
    #[OA\Property(title: "Post status", description: "Post status", type: "string", enum: PostStatusEnum::CASES)]
    private PostStatusEnum $status;

    /** @var string[] */
    #[Groups([SerializationGroup::DETAIL])]
    #[OA\Property(title: "Post tags", description: "Post tags", type: "array", items: new OA\Items(type: "string"))]
    private array $tags;

    public function __construct(Post $post)
    {
        $this->title = $post->getTitle();
        $this->slug = $post->getSlug();
        $this->tags = $post->getTags();
        $this->status = $post->getStatus();
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getStatus(): PostStatusEnum
    {
        return $this->status;
    }

    /** @return string[] */
    public function getTags(): array
    {
        return $this->tags;
    }
}
