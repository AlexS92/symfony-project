<?php
declare(strict_types=1);

namespace App\Api\Response;

use App\Entity\Application;
use App\Enum\ApplicationStatusEnum;
use OpenApi\Attributes as OA;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Api\Enum\SerializationGroup;
use Symfony\Component\Uid\Uuid;

#[OA\Schema(title: "Application", description: "Application")]
readonly class ApplicationResponse
{
    #[Groups([SerializationGroup::DETAIL])]
    #[OA\Property(title: "Application Id", description: "Application Id", type: "string")]
    private Uuid $id;

    #[Groups([SerializationGroup::DETAIL])]
    #[OA\Property(title: "Application name", description: "Application name")]
    private string $name;

    #[Groups([SerializationGroup::DETAIL])]
    #[OA\Property(title: "Application description", description: "Application description")]
    private ?string $description;

    #[Groups([SerializationGroup::DETAIL])]
    #[OA\Property(title: "Application status", description: "Application status", type: "string", enum: ApplicationStatusEnum::CASES)]
    private ApplicationStatusEnum $status;

    #[Groups([SerializationGroup::DETAIL])]
    #[OA\Property(title: "CreatedAt", description: "Created at datetime", type: "string", format: "ISO 8601", nullable: false)]
    private \DateTimeImmutable $createdAt;

    #[Groups([SerializationGroup::DETAIL])]
    #[OA\Property(title: "UpdatedAt", description: "Updated at datetime", type: "string", format: "ISO 8601", nullable: false)]
    private \DateTimeImmutable $updatedAt;

    #[Groups([SerializationGroup::TOKEN])]
    #[OA\Property(title: "Token", description: "token", type: "string", format: "JWT", nullable: false)]
    private ?string $token;

    public function __construct(Application $application)
    {
        $this->id = $application->getId();
        $this->name = $application->getName();
        $this->description = $application->getDescription();
        $this->status = $application->getStatus();
        $this->token = $application->getToken();
        $this->createdAt = $application->getCreatedAt();
        $this->updatedAt = $application->getUpdatedAt();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getStatus(): ApplicationStatusEnum
    {
        return $this->status;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }
}
