<?php
declare(strict_types=1);

namespace App\Api\Response\Error;

use OpenApi\Attributes as OA;

#[OA\Schema(title: "Error", description: "Api Error", required: ["message"])]
readonly class ApiError
{
    #[OA\Property(title: "Message", description: "Error message")]
    private string $message;

    #[OA\Property(title: "Path", description: "Error path")]
    private ?string $path;

    /** @var string|bool|integer|float|array<string>|array<int>|array<bool>|array<float> */
    #[OA\Property(title: "Value", description: "Erroneous value", nullable: true)]
    private mixed $value;

    public function __construct(string $message, ?string $path, mixed $value)
    {
        $this->message = $message;
        $this->path = $path;
        $this->value = $value;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }
}
