<?php
declare(strict_types=1);

namespace App\Api\Response\Error;

use OpenApi\Attributes as OA;

#[OA\Schema(title: "Error Response", description: "Api Error Response", required: ["message", "code", "errors"])]
readonly class ApiErrorResponse
{
    #[OA\Property(title: "Error code", description: "Error code")]
    private int $code;

    #[OA\Property(title: "Message", description: "Error message")]
    private string $message;

    /** @var ApiError[] */
    #[OA\Property(title: "Errors list", description: "Errors that occurred while processing the request")]
    private array $errors;

    /** @param ApiError[] $errors */
    public function __construct(int $code, string $message, array $errors = [])
    {
        $this->code = $code;
        $this->message = $message;
        $this->errors = $errors;
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    /** @return ApiError[] */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
