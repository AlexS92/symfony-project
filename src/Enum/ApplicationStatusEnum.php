<?php
declare(strict_types=1);

namespace App\Enum;

use App\Enum\Helper\EnumValues;

enum ApplicationStatusEnum: string
{
    use EnumValues;

    case ACTIVE = 'active';
    case DISABLE = 'disable';
    case BLOCKED = 'blocked';

    public const array CASES = [self::ACTIVE, self::DISABLE, self::BLOCKED];

    public const string DEFAULT = self::ACTIVE->value;
}
