<?php
declare(strict_types=1);

namespace App\Enum;

use App\Enum\Helper\EnumValues;

enum PostStatusEnum: string
{
    use EnumValues;

    case PUBLIC = 'public';
    case DRAFT = 'draft';
    case PENDING = 'pending';
    case DELETED = 'deleted';
    case BLOCKED = 'blocked';

    public const array CASES = [self::PUBLIC, self::DRAFT, self::PENDING, self::DELETED, self::BLOCKED];

    public const string DEFAULT = self::DRAFT->value;
}
