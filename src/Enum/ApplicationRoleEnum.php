<?php

namespace App\Enum;

use App\Enum\Helper\EnumValues;

enum ApplicationRoleEnum: string
{
    use EnumValues;

    case ROLE_APPLICATION_READ = 'ROLE_APPLICATION_READ';
    case ROLE_APPLICATION_WRITE = 'ROLE_APPLICATION_WRITE';

    public const array CASES = [self::ROLE_APPLICATION_READ, self::ROLE_APPLICATION_WRITE];

    public const string DEFAULT = self::ROLE_APPLICATION_READ->value;
}
