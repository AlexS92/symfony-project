<?php

namespace App\Enum;

use App\Enum\Helper\EnumValues;

enum UserRoleEnum: string
{
    use EnumValues;

    case PUBLIC_ACCESS = 'PUBLIC_ACCESS';
    case IS_AUTHENTICATED_FULLY = 'IS_AUTHENTICATED_FULLY';

    case ROLE_ADMIN = 'ROLE_ADMIN';
    case ROLE_USER = 'ROLE_USER';

    public const array CASES = [self::ROLE_ADMIN, self::ROLE_USER];

    public const string DEFAULT = self::ROLE_USER->value;
}
