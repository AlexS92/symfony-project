<?php
declare(strict_types=1);

namespace App\Enum\Helper;

/** @see \UnitEnum::cases() */
trait TryFromForPureCaseEnum
{
    public static function from(string $value): static
    {
        try {
            return constant(static::class . '::' . $value);
        } catch (\Throwable $exception) {
            throw new \ValueError(
                message: "{$value} is not a valid case name for enum " . static::class,
                previous: $exception
            );
        }
    }

    public static function tryFrom(string $value): ?static
    {
        $cases = array_column(static::cases(), 'name');
        if (in_array($value, $cases, true)) {
            return constant(static::class . '::' . $value);
        }

        return null;
    }
}
