<?php
declare(strict_types=1);

namespace App\Enum\Helper;

/** @see \UnitEnum::cases() */
trait EnumValues
{
    /** @return int[]|string[] */
    public static function values(): array
    {
        if (is_subclass_of(static::class, \BackedEnum::class)) {
            return array_column(static::cases(), 'value');
        }

        /** @phpstan-ignore-next-line */
        return [];
    }

    /** @return array<string,string>|array<string, int> */
    public static function map(): array
    {
        if (is_subclass_of(static::class, \BackedEnum::class)) {
            return array_column(static::cases(), 'value', 'name');
        }

        /** @phpstan-ignore-next-line */
        return [];
    }
}
