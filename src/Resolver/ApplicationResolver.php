<?php
declare(strict_types=1);

namespace App\Resolver;

use App\Entity\Application;
use App\Security\ApplicationSecurityDto\ApplicationToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

readonly class ApplicationResolver implements ValueResolverInterface
{
    private TokenStorageInterface $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /** @return \Generator<Application> */
    #[\Override]
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (!$this->supports($argument)) {
            return [];
        }

        /** @var ApplicationToken $token */
        $token = $this->tokenStorage->getToken();

        yield $token->getApplication();
    }

    private function supports(ArgumentMetadata $argument): bool
    {
        $token = $this->tokenStorage->getToken();
        if (is_object($token)) {
            return
                is_a((string)$argument->getType(), Application::class, true) &&
                is_a($token, ApplicationToken::class);
        }

        return false;
    }
}
