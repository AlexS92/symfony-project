<?php
declare(strict_types=1);

namespace App\Resolver;

use App\Api\Request\BodyRequestDtoMarker;
use App\Event\BeforeDtoValidationEvent;
use App\Exception\HttpApiValidationException;
use Ramsey\Collection\CollectionInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

readonly class RequestDtoResolver implements ValueResolverInterface
{
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;
    private EventDispatcherInterface $dispatcher;

    public function __construct(
        SerializerInterface      $serializer,
        ValidatorInterface       $validator,
        EventDispatcherInterface $dispatcher,
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @return \Generator<BodyRequestDtoMarker>|\Generator<CollectionInterface>
     * @throws HttpApiValidationException
     */
    #[\Override]
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (!$this->supports($argument)) {
            return [];
        }

        $content = $request->getContent();

        try {
            $dto = $this->serializer->deserialize($content, (string)$argument->getType(), JsonEncoder::FORMAT);
        } catch (\Throwable) {
            throw new HttpApiValidationException("Bad Request", Response::HTTP_BAD_REQUEST);
        }

        $event = new BeforeDtoValidationEvent($request, $dto);
        $this->dispatcher->dispatch($event, BeforeDtoValidationEvent::NAME);
        $groups = $event->getValidationGroups();

        $violationList = $this->validator->validate($dto, null, $groups);
        if ($violationList->count() > 0) {
            throw new HttpApiValidationException("Bad Request", Response::HTTP_BAD_REQUEST, constraintViolationList: $violationList);
        }

        yield $dto;
    }

    private function supports(ArgumentMetadata $argument): bool
    {
        $type = (string)$argument->getType();

        return
            is_subclass_of($type, BodyRequestDtoMarker::class) ||
            is_subclass_of($type, CollectionInterface::class);
    }
}
