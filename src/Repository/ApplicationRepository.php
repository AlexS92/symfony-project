<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Application;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Application|null find($id, $lockMode = null, $lockVersion = null)
 * @method Application|null findOneBy(array $criteria, array $orderBy = null)
 * @method Application[]    findAll()
 * @method Application[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Application::class);
    }

    public function findOneByToken(string $token): ?Application
    {
        return $this->findOneBy(['token' => $token]);
    }

    public function save(Application $app): void
    {
        $this->_em->persist($app);
        $this->_em->flush();
    }

    public function remove(Application $app): void
    {
        $this->_em->remove($app);
        $this->_em->flush();
    }
}
