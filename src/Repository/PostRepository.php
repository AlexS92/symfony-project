<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Application;
use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\Persistence\ManagerRegistry;
use MartinGeorgiev\Doctrine\DBAL\Types\TextArray;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function save(Post $app): void
    {
        $this->_em->persist($app);
        $this->_em->flush();
    }

    public function remove(Post $app): void
    {
        $this->_em->remove($app);
        $this->_em->flush();
    }

    /**
     * @param array<int, array<string, mixed>> $arrayMapList
     * @return Post[]
     * @throws DBALException
     */
    public function insertOrUpdate(array $arrayMapList, Application $application): array
    {
        $columns = ['application_id'];
        $set = ['?'];
        foreach ($arrayMapList[0] as $column => $_) {
            $columns[] = $column;
            $set[] = '?';
        }

        $valueSet = '(' . implode(',', $set) . ')';

        $valueSets = [];
        $values = [];
        foreach ($arrayMapList as $map) {
            $valueSets[] = $valueSet;
            $values[] = $application->getId();
            foreach ($map as $_ => $value) {
                if (is_array($value)) {
                    $value = $this->convertToTextArrayValue($value);
                }
                $values[] = $value;
            }
        }

        $sql = '
            INSERT INTO posts (' . implode(', ', $columns) . ') 
            VALUES ' . implode(', ', $valueSets) . '
            ON CONFLICT ON CONSTRAINT slug_application_uniq DO NOTHING
            RETURNING id;
        ';

        $connection = $this->_em->getConnection();
        $stmt = $connection->prepare($sql);
        $result = $stmt->executeQuery($values);

        $idList = array_column($result->fetchAllAssociative(), 'id');

        return $this->findBy(['id' => $idList]);
    }

    /** @param string[] $value */
    private function convertToTextArrayValue(array $value): string
    {
        return (new TextArray())
                ->convertToDatabaseValue($value, new PostgreSQLPlatform()) ?? 'array[]::citext[]';
    }
}
