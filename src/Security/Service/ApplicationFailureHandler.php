<?php
declare(strict_types=1);

namespace App\Security\Service;

use App\Api\Responder\ApiErrorResponder;
use App\Api\Response\Error\ApiErrorResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

readonly class ApplicationFailureHandler implements AuthenticationFailureHandlerInterface
{
    private ApiErrorResponder $apiErrorResponder;

    public function __construct(ApiErrorResponder $apiErrorResponder)
    {
        $this->apiErrorResponder = $apiErrorResponder;
    }

    #[\Override]
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        $response = new ApiErrorResponse($exception->getCode(), $exception->getMessage());

        return $this->apiErrorResponder->createResponse($response, Response::HTTP_UNAUTHORIZED);
    }
}
