<?php
declare(strict_types=1);

namespace App\Security\Service;

use App\Repository\ApplicationRepository;
use App\Security\ApplicationSecurityDto\ApplicationAsUser;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

readonly class ApplicationUserProvider implements UserProviderInterface
{
    private ApplicationRepository $applicationRepository;

    public function __construct(ApplicationRepository $applicationRepository)
    {
        $this->applicationRepository = $applicationRepository;
    }

    #[\Override]
    public function refreshUser(UserInterface $user): UserInterface
    {
        if ($user instanceof ApplicationAsUser) {
            $application = $this->applicationRepository->findOneByToken((string)$user->getApplication()->getToken()) ??
                throw new BadCredentialsException('Application not found.');

            return new ApplicationAsUser($application);
        }

        throw new BadCredentialsException('ApplicationBadge token check failed.');
    }

    #[\Override]
    public function supportsClass(string $class): bool
    {
        return ApplicationAsUser::class === $class;
    }

    #[\Override]
    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $application = $this->applicationRepository->findOneByToken($identifier) ??
            throw new BadCredentialsException('Application not found.');

        return new ApplicationAsUser($application);
    }
}
