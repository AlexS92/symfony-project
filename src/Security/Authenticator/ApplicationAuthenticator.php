<?php
declare(strict_types=1);

namespace App\Security\Authenticator;

use App\Security\ApplicationSecurityDto\ApplicationBadge;
use App\Security\ApplicationSecurityDto\ApplicationPassport;
use App\Security\ApplicationSecurityDto\ApplicationToken;
use App\Security\ApplicationSecurityDto\ApplicationAsUser;
use App\Security\Service\ApplicationUserProvider;
use App\Service\Shared\JwtService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\AccessToken\AccessTokenExtractorInterface;
use Symfony\Component\Security\Http\AccessToken\HeaderAccessTokenExtractor;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authenticator\AuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

readonly class ApplicationAuthenticator implements AuthenticatorInterface
{
    private JwtService $jwtService;
    private ApplicationUserProvider $applicationUserProvider;
    private AuthenticationFailureHandlerInterface $failureHandler;
    private AccessTokenExtractorInterface $tokenExtractor;

    public function __construct(JwtService $jwtService, ApplicationUserProvider $applicationUserProvider, AuthenticationFailureHandlerInterface $failureHandler)
    {
        $this->jwtService = $jwtService;
        $this->applicationUserProvider = $applicationUserProvider;
        $this->failureHandler = $failureHandler;

        $this->tokenExtractor = new HeaderAccessTokenExtractor('X-API-KEY', '');
    }

    #[\Override]
    public function supports(Request $request): bool
    {
        return is_string($this->tokenExtractor->extractAccessToken($request));
    }

    #[\Override]
    public function authenticate(Request $request): Passport
    {
        $jwtString = $this->tokenExtractor->extractAccessToken($request) ?? '';

        $checker = $this->jwtService->isTokenValid(...);

        return new ApplicationPassport(
            new ApplicationBadge($jwtString, $checker(...)),
            new UserBadge($jwtString, $this->applicationUserProvider->loadUserByIdentifier(...))
        );
    }

    #[\Override]
    public function createToken(Passport $passport, string $firewallName): TokenInterface
    {
        $badge = $passport->getBadge(ApplicationBadge::class) ??
            throw new \LogicException('This Authenticator supports only ApplicationBadge');

        $applicationUser = $this->applicationUserProvider->loadUserByIdentifier($badge->getXApiToken());
        if ($applicationUser instanceof ApplicationAsUser) {
            return new ApplicationToken($applicationUser);
        }

        throw new \LogicException('This Authenticator supports only ' . ApplicationAsUser::class . ' class.');
    }

    #[\Override]
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    #[\Override]
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return $this->failureHandler->onAuthenticationFailure($request, $exception);
    }
}
