<?php
declare(strict_types=1);

namespace App\Security\EventSubscriber;

use App\Security\ApplicationSecurityDto\ApplicationBadge;
use App\Security\ApplicationSecurityDto\ApplicationToken;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Http\Event\CheckPassportEvent;

class CheckApplicationPassportListener implements EventSubscriberInterface
{
    #[\Override]
    public static function getSubscribedEvents(): array
    {
        return [
            CheckPassportEvent::class => ['checkApplicationPassport'], // 999
//            AuthenticationSuccessEvent::class => ['applicationTokenStopPropagation', 999],
        ];
    }

    public function checkApplicationPassport(CheckPassportEvent $event): void
    {
        $passport = $event->getPassport();
        if ($passport->hasBadge(ApplicationBadge::class)) {
            /** @var ApplicationBadge $badge */
            $badge = $passport->getBadge(ApplicationBadge::class);
            if ($badge->isResolved()) {
                return;
            }

            $badge->checkApplicationToken();
            //$event->stopPropagation();
        }
    }

//    public function applicationTokenStopPropagation(AuthenticationSuccessEvent $event): void
//    {
//        if (is_a($event->getAuthenticationToken(), ApplicationToken::class)) {
//            $event->stopPropagation();
//        }
//    }
}
