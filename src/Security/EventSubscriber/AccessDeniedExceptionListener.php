<?php

namespace App\Security\EventSubscriber;

use App\Api\Responder\ApiErrorResponder;
use App\Api\Response\Error\ApiErrorResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

readonly class AccessDeniedExceptionListener implements EventSubscriberInterface
{
    private TokenStorageInterface $tokenStorage;
    private ApiErrorResponder $apiErrorResponder;
    private AuthenticationTrustResolverInterface $authenticationTrustResolver;

    public function __construct(TokenStorageInterface $tokenStorage, ApiErrorResponder $apiErrorResponder, AuthenticationTrustResolverInterface $authenticationTrustResolver)
    {
        $this->tokenStorage = $tokenStorage;
        $this->apiErrorResponder = $apiErrorResponder;
        $this->authenticationTrustResolver = $authenticationTrustResolver;
    }

    #[\Override]
    public static function getSubscribedEvents(): array
    {
        return [
            ExceptionEvent::class => ['onKernelException', 999],
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if ($exception instanceof AccessDeniedException) {
            $token = $this->tokenStorage->getToken();
            if (!$this->authenticationTrustResolver->isFullFledged($token)) {
                $response = new ApiErrorResponse(Response::HTTP_UNAUTHORIZED, 'Full authentication is required to access this resource.');

                $event->setResponse($this->apiErrorResponder->createResponse($response, Response::HTTP_UNAUTHORIZED));
                $event->stopPropagation();
                return;
            }

            $response = new ApiErrorResponse(Response::HTTP_FORBIDDEN, "Access Denied.");
            $event->setResponse($this->apiErrorResponder->createResponse($response, Response::HTTP_FORBIDDEN));
            $event->stopPropagation();
        }
    }
}
