<?php
declare(strict_types=1);

namespace App\Security\EventSubscriber;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Http\Authenticator\JsonLoginAuthenticator;
use Symfony\Component\Security\Http\Event\CheckPassportEvent;

class AuthenticationSuccessEventListener implements EventSubscriberInterface
{
    #[\Override]
    public static function getSubscribedEvents(): array
    {
        return [
            CheckPassportEvent::class => 'checkUser'
        ];
    }

    /** @throws UserNotFoundException */
    public function checkUser(CheckPassportEvent $event): void
    {
        $user = $event->getPassport()->getUser();
        $authenticator = $event->getAuthenticator();
        if (!$authenticator instanceof JsonLoginAuthenticator || !$user instanceof User) {
            return;
        }

        if (null !== $user->getDeletedAt()) {
            $exception = new UserNotFoundException();
            $exception->setUserIdentifier($user->getUserIdentifier());

            throw $exception;
        }
    }
}
