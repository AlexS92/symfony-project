<?php
declare(strict_types=1);

namespace App\Security\ApplicationSecurityDto;

use Symfony\Component\Security\Http\Authenticator\Passport\Badge\BadgeInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

class ApplicationPassport extends Passport
{
    /** @param BadgeInterface[] $badges */
    public function __construct(ApplicationBadge $applicationBadge, UserBadge $userBadge, array $badges = [])
    {
        $this->addBadge($applicationBadge);
        $this->addBadge($userBadge);

        foreach ($badges as $badge) {
            $this->addBadge($badge);
        }
    }
}
