<?php
declare(strict_types=1);

namespace App\Security\ApplicationSecurityDto;

use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\BadgeInterface;

class ApplicationBadge implements BadgeInterface
{
    private readonly \Closure $tokenChecker;
    private bool $isResolved = false;

    private readonly string $xApiToken;

    public function __construct(string $xApiToken, callable $tokenChecker)
    {
        $this->tokenChecker = $tokenChecker(...);
        $this->xApiToken = $xApiToken;
    }

    public function getXApiToken(): string
    {
        return $this->xApiToken;
    }

    #[\Override]
    public function isResolved(): bool
    {
        return $this->isResolved;
    }

    public function checkApplicationToken(): void
    {
        if (!($this->tokenChecker)($this->xApiToken)) {
            throw new BadCredentialsException('ApplicationBadge token check failed');
        }

        $this->isResolved = true;
    }
}
