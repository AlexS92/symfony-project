<?php
declare(strict_types=1);

namespace App\Security\ApplicationSecurityDto;

use App\Entity\Application;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ApplicationToken implements TokenInterface
{
    private ApplicationAsUser $user;

    /** @var array<string, mixed> $attributes */
    private array $attributes;

    /** @param array<string, mixed> $attributes */
    public function __construct(ApplicationAsUser $applicationAsUser, array $attributes = [])
    {
        $this->user = $applicationAsUser;
        $this->attributes = $attributes;
    }

    public function getApplication(): Application
    {
        return $this->user->getApplication();
    }

    #[\Override]
    public function __toString(): string
    {
        return $this->user->getApplication()->getToken() ?? '';
    }

    #[\Override]
    public function getUserIdentifier(): string
    {
        return $this->user->getApplication()->getToken() ?? '';
    }

    /** @return string[] */
    #[\Override]
    public function getRoleNames(): array
    {
        return $this->user->getApplication()->getRoles();
    }

    #[\Override]
    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    #[\Override]
    public function setUser(UserInterface $user): void
    {
        if (!$user instanceof ApplicationAsUser) {
            throw new \LogicException('This ' . ApplicationToken::class . ' supports only ' . ApplicationAsUser::class . ' class.');
        }

        $this->user = $user;
    }

    #[\Override]
    public function eraseCredentials(): void
    {
        $this->user->eraseCredentials();
    }

    /** @return array<string, mixed> */
    #[\Override]
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /** @param array<string, mixed> $attributes */
    #[\Override]
    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    #[\Override]
    public function hasAttribute(string $name): bool
    {
        return \array_key_exists($name, $this->attributes);
    }

    #[\Override]
    public function getAttribute(string $name): mixed
    {
        return $this->attributes[$name] ??
            throw new \InvalidArgumentException(sprintf('This token has no "%s" attribute.', $name));
    }

    #[\Override]
    public function setAttribute(string $name, mixed $value): void
    {
        $this->attributes[$name] = $value;
    }

    #[\Override]
    public function __serialize(): array
    {
        return [$this->user];
    }

    /** @param mixed[] $data */
    #[\Override]
    public function __unserialize(array $data): void
    {
        [$this->user] = $data;
    }
}
