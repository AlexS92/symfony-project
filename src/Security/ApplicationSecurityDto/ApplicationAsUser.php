<?php
declare(strict_types=1);

namespace App\Security\ApplicationSecurityDto;

use App\Entity\Application;
use Symfony\Component\Security\Core\User\UserInterface;

readonly class ApplicationAsUser implements UserInterface
{
    private Application $application;

    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    #[\Override]
    /** @return string[] */
    public function getRoles(): array
    {
        return $this->application->getRoles();
    }

    #[\Override]
    public function eraseCredentials(): void
    {
    }

    #[\Override]
    public function getUserIdentifier(): string
    {
        return $this->application->getToken() ?? '';
    }

    public function getApplication(): Application
    {
        return $this->application;
    }
}
