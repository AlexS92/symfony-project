<?php
declare(strict_types=1);

namespace App\Serializer;

use Ramsey\Collection\CollectionInterface;
use Ramsey\Collection\Exception\InvalidArgumentException as RamseyInvalidArgumentException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ClassCollectionNormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    #[\Override]
    public function supportsDenormalization(mixed $data, string $type, ?string $format = null): bool
    {
        return is_subclass_of($type, CollectionInterface::class) && \is_array($data);
    }

    /**
     * @param array<string, mixed> $data
     * @param array<string, mixed> $context
     * @throws ExceptionInterface
     */
    #[\Override]
    public function denormalize($data, string $type, ?string $format = null, array $context = []): CollectionInterface
    {
        try {
            $collection = $this->createAndFillCollection($data, $type, $format, $context);
        } catch (RamseyInvalidArgumentException $exception) {
            throw new InvalidArgumentException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $collection;
    }

    /**
     * @param array<string, mixed> $data
     * @param array<string, mixed> $context
     * @throws ExceptionInterface
     */
    private function createAndFillCollection(array $data, string $class, ?string $format, array $context = []): CollectionInterface
    {
        /** @var CollectionInterface $collection */
        $collection = new $class();

        $itemType = $collection->getType();

        if (class_exists($itemType) || interface_exists($itemType)) {
            foreach ($data as $item) {
                $item = $this->denormalizer->denormalize($item, $itemType, $format, $context);
                $collection->add($item);
            }
        } else {
            throw new InvalidArgumentException("Undefined class `{$itemType}`");
        }

        return $collection;
    }
}
