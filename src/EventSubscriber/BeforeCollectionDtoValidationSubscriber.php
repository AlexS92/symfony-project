<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use App\Event\BeforeDtoValidationEvent;
use Ramsey\Collection\CollectionInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BeforeCollectionDtoValidationSubscriber implements EventSubscriberInterface
{
    #[\Override]
    public static function getSubscribedEvents(): array
    {
        return [
            BeforeDtoValidationEvent::NAME => ['setValidationGroups']
        ];
    }

    /** @throws \ReflectionException */
    public function setValidationGroups(BeforeDtoValidationEvent $event): void
    {
        $dto = $event->getDto();
        if (!is_subclass_of($dto, CollectionInterface::class)) {
            return;
        }

        /** @phpstan-ignore-next-line */
        $groups = [(new \ReflectionClass($dto->getType()))->getShortName()]; //todo подумать где может находить код, т.к. тут мы знаем только одну группу валидации - дефолтную
        $event->setValidationGroups($groups);
    }
}
