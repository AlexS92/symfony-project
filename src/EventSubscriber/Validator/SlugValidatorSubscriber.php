<?php
declare(strict_types=1);

namespace App\EventSubscriber\Validator;

use App\Exception\HttpApiValidationException;
use App\Validator\Service\ConstraintViolationManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

readonly class SlugValidatorSubscriber implements EventSubscriberInterface
{
    private ValidatorInterface $validator;
    private ConstraintViolationManager $constraintViolationManager;
    private string $regExpSlug;

    public function __construct(
        ValidatorInterface $validator,
        ConstraintViolationManager $constraintViolationManager,
        string $regExpSlug
    ) {
        $this->validator = $validator;
        $this->constraintViolationManager = $constraintViolationManager;
        $this->regExpSlug = $regExpSlug;
    }

    #[\Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', -256],
        ];
    }

    /** @throws HttpApiValidationException */
    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $slug = $request->attributes->get('slug');

        if (null !== $slug) {
            $regexp = new Regex($this->regExpSlug);
            $regexp->message = 'The slug is not valid: valid symbols are: ' . $this->regExpSlug;

            $constraintViolationList = $this->validator->validate($slug, $regexp);

            if ($constraintViolationList->count() > 0) {
                /** @var ConstraintViolation $constraintViolation */
                foreach ($constraintViolationList as $constraintViolation) {
                    $this->constraintViolationManager->setPathAndValueToConstraintViolation($constraintViolation, 'slug', $slug);
                }

                throw new HttpApiValidationException("Bad request", Response::HTTP_BAD_REQUEST, constraintViolationList: $constraintViolationList);
            }
        }
    }
}
