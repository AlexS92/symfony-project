<?php
declare(strict_types=1);

namespace App\EventSubscriber\ExceptionSubscriber;

use App\Api\Responder\ApiErrorResponder;
use App\Api\Response\Error\ApiErrorResponse;
use App\Exception\DomainException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

readonly class DomainExceptionHandler implements EventSubscriberInterface
{
    private ApiErrorResponder $apiErrorResponder;

    public function __construct(ApiErrorResponder $apiErrorResponder)
    {
        $this->apiErrorResponder = $apiErrorResponder;
    }

    #[\Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['onDomainException'],
        ];
    }

    public function onDomainException(ExceptionEvent $event): void
    {
        /** @var DomainException $exception */
        $exception = $event->getThrowable();
        if (DomainException::class === $exception::class) {
            $response = new ApiErrorResponse($exception->getCode(), $exception->getMessage());
            $httpResponseCode = (0 === $exception->getCode()) ? Response::HTTP_BAD_REQUEST : $exception->getCode();
            $event->setResponse(
                $this->apiErrorResponder->createResponse($response, $httpResponseCode)
            );
        }
    }
}
