<?php
declare(strict_types=1);

namespace App\EventSubscriber\ExceptionSubscriber;

use App\Api\Responder\ApiErrorResponder;
use App\Api\Response\Error\ApiErrorResponse;
use App\Exception\HttpApiValidationException;
use App\Validator\Service\ConstraintViolationListParser;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

readonly class HttpApiValidationExceptionHandler implements EventSubscriberInterface
{
    private ApiErrorResponder $apiErrorResponder;
    private ConstraintViolationListParser $constraintViolationListParser;

    public function __construct(ApiErrorResponder $apiErrorResponder, ConstraintViolationListParser $constraintViolationListParser)
    {
        $this->apiErrorResponder = $apiErrorResponder;
        $this->constraintViolationListParser = $constraintViolationListParser;
    }

    #[\Override]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['onHttpApiException'],
        ];
    }

    public function onHttpApiException(ExceptionEvent $event): void
    {
        /** @var HttpApiValidationException $exception */
        $exception = $event->getThrowable();
        if (HttpApiValidationException::class === $exception::class) {
            $httpResponseCode = (0 === $exception->getCode()) ? Response::HTTP_BAD_REQUEST : $exception->getCode();
            $violationList = $exception->getConstraintViolationList();
            $errors = match ($violationList) {
                null => [],
                default => $this->constraintViolationListParser->parseToApiErrorList($violationList),
            };

            $response = new ApiErrorResponse($exception->getCode(), $exception->getMessage(), $errors);

            $event->setResponse(
                $this->apiErrorResponder->createResponse($response, $httpResponseCode)
            );
        }
    }
}
