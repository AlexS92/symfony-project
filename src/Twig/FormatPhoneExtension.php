<?php
declare(strict_types=1);

namespace App\Twig;

use App\Service\Shared\PhoneUtilService;
use libphonenumber\NumberParseException;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class FormatPhoneExtension extends AbstractExtension
{
    private readonly PhoneUtilService $phoneUtilService;

    public function __construct(PhoneUtilService $phoneUtilService)
    {
        $this->phoneUtilService = $phoneUtilService;
    }

    #[\Override]
    public function getFilters(): array
    {
        return [
            new TwigFilter('phoneNumber', $this->formatPhoneNumber(...)),
        ];
    }

    public function formatPhoneNumber(string $phoneNumber, ?string $regionCode = null): string
    {
        try {
            $phoneNumber = $this->phoneUtilService->parseToPhoneNumber($phoneNumber, $regionCode);
            return $this->phoneUtilService->format($phoneNumber);
        } catch (NumberParseException) {
            return $phoneNumber;
        }
    }
}
