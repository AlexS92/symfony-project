<?php
declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'console:data:chart', description: 'Draw th draft by data')]
class DataChartCommand extends Command
{
    #[\Override]
    protected function configure(): void
    {
        /** @phpstan-ignore-next-line */
        $this
            ->setDescription('Draw th draft by data')
            ->addOption(
                'vertical-scale',
                'vs',
                InputOption::VALUE_OPTIONAL,
                'This is a vertical scale multiplayer of the dart, a positive value below 4',
                2
            )
            ->addArgument(
                'chart-data',
                InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                'Array of dots',
                [
                    1.1066, 1.1048, 1.1023, 1.1003, 1.0969, 1.0951, 1.0901, 1.0914, 1.0867, 1.0842, 1.0835, 1.0816, 1.08, 1.079,
                    1.0801, 1.0818, 1.084, 1.0875, 1.0964, 1.0977, 1.1122, 1.1117, 1.1125, 1.1187, 1.1336, 1.1456, 1.139, 1.1336,
                    1.124, 1.1104, 1.1157, 1.0982, 1.0934, 1.0801, 1.0707, 1.0783, 1.0843, 1.0827, 1.0981, 1.0977, 1.1034, 1.0956,
                    1.0936, 1.0906, 1.0785, 1.0791, 1.0885, 1.0871, 1.0867, 1.0963,
                ]
            );
    }

    #[\Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $data = $input->getArgument('chart-data');
        $data = array_map(fn($value) => floatval($value), $data);
        if (empty($data)) {
            $output->writeln('Array of data must not be empty');
            return self::INVALID;
        }

        $verticalScale = (int)$input->getOption('vertical-scale');
        if ($verticalScale < 1 || $verticalScale > 4) {
            $output->writeln('Vertical scale must be positive value or lesser then 4');
            return self::INVALID;
        }

        $minValue = min($data);
        $maxValue = max($data);

        $count = count($data);

        $countSymbolsAfterComma = $this->getCountSymbolsAfterComma($minValue) - 1; // todo целые числа
        $precision = $this->getPrecision($verticalScale, $countSymbolsAfterComma); // todo нужно брать с наибольшим кол-вом знаков

        $startPoint = $this->getPoint($minValue, $countSymbolsAfterComma) - $precision;
        $endPoint = $this->getEndPoint($maxValue, $precision, $countSymbolsAfterComma);

        $verticalSteps = (int)ceil(($endPoint - $startPoint) / $precision);

        $newData = [];
        foreach ($data as $position => $point) {
            $lineNumber = (int)round(round(($this->getPoint($point, $countSymbolsAfterComma) - $startPoint) / $precision, $countSymbolsAfterComma));
            $newData[$lineNumber][] = [$position + 1 => $point];
        }

        for ($i = $verticalSteps; $i >= 1; $i--) {
            $point = $startPoint + $precision * $i;

            $output->write((string)$point);
            $output->write("\t ");
            $output->write("\u{251C}");

            $dotsOnThisLine = $newData[$i] ?? [];

            $prevPosition = 0;
            if (empty($dotsOnThisLine)) {
                $output->write(str_repeat("\u{2591}\u{2591}\u{2591}", $count));
            } else {
                foreach ($dotsOnThisLine as $dot) {
                    $position = key($dot);
                    $output->write(str_repeat("\u{2591}\u{2591}\u{2591}", $position - $prevPosition - 1));
                    $output->write("\u{2591}*\u{2591}");
                    $prevPosition = $position;
                }

                $output->write(str_repeat("\u{2591}\u{2591}\u{2591}", $count - $prevPosition));
            }

            $output->writeln('');
        }

        $output->write((string)$startPoint);
        $output->write("\t ");
        $output->write("\u{2514}");
        $output->writeln(str_repeat("\u{2500}\u{2534}\u{2500}", $count));

        $output->write('0');
        $output->write("\t  ");
        $output->write(implode(' ', array_map(
            fn(int $val) => sprintf('%02d', $val),
            range(1, $count)
        )));

        $output->writeln('');

        return self::SUCCESS;
    }

    private function getPoint(float $value, int $countSymbolsAfterComma): float
    {
        return (float)bcdiv((string)$value, '1', $countSymbolsAfterComma);
    }

    private function getEndPoint(float $maxValue, float $precision, int $countSymbolsAfterComma): float
    {
        $endPoint = round($maxValue, $countSymbolsAfterComma);
        if ($maxValue > $endPoint) {
            $endPoint += $precision;
        }

        return $endPoint;
    }

    private function getCountSymbolsAfterComma(float $number): int
    {
        $e = 1;
        while ((round($number * $e) / $e) !== $number) {
            $e *= 10;
        }

        return (int)log($e, 10);
    }

    private function getPrecision(int $verticalScale, int $countSymbolsAfterComma): float
    {
        return ($verticalScale / (10 ** $countSymbolsAfterComma));
    }
}
