<?php
declare(strict_types=1);

namespace App\Controller;

use App\Api\Request\CreateApplicationDto;
use App\Api\Responder\ApiResponder;
use App\Entity\User;
use App\Enum\ApplicationStatusEnum;
use App\Enum\UserRoleEnum;
use App\Service\ApplicationService;
use OpenApi\Attributes as OA;
use App\Api\Enum\SerializationGroup;
use App\Api\Response\ApplicationResponse;
use App\Api\Response\Error\ApiErrorResponse;
use App\Entity\Application;
use App\Exception\DomainException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[OA\Tag(name: "Application")]
#[Route(path: "/applications")]
class ApplicationController extends AbstractController
{
    private ApiResponder $responder;
    private ApplicationService $applicationService;

    public function __construct(ApiResponder $responder, ApplicationService $applicationService)
    {
        $this->responder = $responder;
        $this->applicationService = $applicationService;
    }

    /** @throws DomainException */
    #[OA\Post(description: "Create Application")]
    #[OA\RequestBody(
        request: "CreateApplicationDto",
        required: true,
        content: new OA\JsonContent(ref: new Model(type: CreateApplicationDto::class))
    )]
    #[OA\Response(
        response: 200,
        description: "Application",
        content: new OA\JsonContent(
            ref: new Model(type: ApplicationResponse::class, groups: [SerializationGroup::DETAIL])
        )
    )]
    #[OA\Response(
        response: 400,
        description: "Error",
        content: new OA\JsonContent(
            type: "array",
            items: new OA\Items(ref: new Model(type: ApiErrorResponse::class)),
            minItems: 1
        )
    )]
    #[Route(name: "create-application", methods: ["POST"])]
    public function createApplication(#[CurrentUser] User $user, CreateApplicationDto $createApplicationDto): JsonResponse
    {
        $application = $this->applicationService->createApplication($user, $createApplicationDto);
        return $this->responder->createResponse(new ApplicationResponse($application), serializerContext: [
            AbstractNormalizer::GROUPS => [SerializationGroup::DETAIL]
        ]);
    }

    #[OA\Get(description: "Get User Application List")]
    #[OA\Response(
        response: 200,
        description: "Application List",
        content: new OA\JsonContent(
            type: "array",
            items: new OA\Items(
                ref: new Model(type: ApplicationResponse::class, groups: [SerializationGroup::DETAIL])
            )
        )
    )]
    #[Route(name: "get-application-list", methods: ["GET"])]
    public function getApplicationList(#[CurrentUser] User $user): JsonResponse
    {
        $applicationCollection = $user->getApplications();

        $response = [];
        foreach ($applicationCollection as $application) {
            $response[] = new ApplicationResponse($application);
        }

        return $this->responder->createResponse($response, serializerContext: [
            AbstractNormalizer::GROUPS => [SerializationGroup::DETAIL]
        ]);
    }

    #[OA\Patch(description: "Change Application status")]
    #[Route(path: '/{id}/{status}', name: "patch-application-status", methods: ["PATCH"])]
    #[IsGranted(
        attribute: new Expression('is_granted("' . UserRoleEnum::ROLE_ADMIN->value . '") or 0 === user.getApplication().getId().compare(subject)'),
        subject: new Expression('args["application"].getId()')
    )]
    public function deleteApplication(#[CurrentUser] User $user, Application $application, ApplicationStatusEnum $status): JsonResponse
    {
        var_dump($application, $status);die();
    }

    /** @throws DomainException */
    #[OA\Get(description: "Generate application token")]
    #[OA\Response(
        response: 200,
        description: "Generate application token",
        content: new OA\JsonContent(
            ref: new Model(type: ApplicationResponse::class, groups: [SerializationGroup::TOKEN])
        )
    )]
    #[OA\Response(
        response: 400,
        description: "Error",
        content: new OA\JsonContent(
            type: "array",
            items: new OA\Items(ref: new Model(type: ApiErrorResponse::class)),
            minItems: 1
        )
    )]
    #[Route(path: "/{id}/generate-token", name: "generate-token", methods: ["GET"])]
    public function generateApplicationToken(Application $application): JsonResponse
    {
        $application = $this->applicationService->generateApplicationToken($application);
        return $this->responder->createResponse(new ApplicationResponse($application), serializerContext: [
            AbstractNormalizer::GROUPS => [SerializationGroup::TOKEN]
        ]);
    }
}
