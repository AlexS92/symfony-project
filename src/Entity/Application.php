<?php
declare(strict_types=1);

namespace App\Entity;

use App\Enum\ApplicationRoleEnum;
use App\Enum\ApplicationStatusEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ApplicationRepository;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

#[ORM\Table(
    name: "applications",
    uniqueConstraints: [
        new ORM\UniqueConstraint(name: "applications_name_user_uniq", columns: ["name", "user_id"])
    ]
)]
#[ORM\Entity(repositoryClass: ApplicationRepository::class)]
class Application
{
    #[ORM\Id]
    #[ORM\Column(name: "id", type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?Uuid $id = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'applications')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    private ?UserInterface $user;

    #[ORM\Column(name: "name", type: Types::STRING, length: 255, nullable: false)]
    private string $name;

    #[ORM\Column(name: "description", type: Types::STRING, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(name: "created_at", type: Types::DATETIMETZ_IMMUTABLE, nullable: false)]
    private \DateTimeImmutable $createdAt;

    #[ORM\Column(name: "updated_at", type: Types::DATETIMETZ_IMMUTABLE, nullable: false)]
    private \DateTimeImmutable $updatedAt;

    #[ORM\Column(name: "deleted_at", type: Types::DATETIMETZ_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    #[ORM\Column(name: "token", type: Types::STRING, length: 512, nullable: true)]
    private ?string $token = null;

    #[ORM\Column(name: "status", type: Types::STRING, nullable: false, enumType: ApplicationStatusEnum::class)]
    private ApplicationStatusEnum $status;

    /** @var string[] */
    #[ORM\Column(name: "roles", type: "text[]", nullable: false)]
    private array $roles = [ApplicationRoleEnum::DEFAULT];

    #[ORM\OneToMany(mappedBy: 'application', targetEntity: Post::class, cascade: ['remove'], orphanRemoval: true)]
    private Collection $posts;

    public function __construct(string $name, ?ApplicationStatusEnum $status = null)
    {
        $this->name = $name;
        $this->status = $status ?? ApplicationStatusEnum::from(ApplicationStatusEnum::DEFAULT);

        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();

        $this->posts = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): void
    {
        $this->token = $token;
    }

    public function getStatus(): ApplicationStatusEnum
    {
        return $this->status;
    }

    public function setStatus(ApplicationStatusEnum $status): void
    {
        $this->status = $status;
    }

    /** @return string[] */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /** @throws \ValueError */
    public function addRole(ApplicationRoleEnum|string $role): void
    {
        $role = is_string($role) ? ApplicationRoleEnum::from($role) : $role;

        $this->roles[] = $role->value;
        $this->roles = array_unique($this->roles);
    }

    /** @throws \ValueError */
    public function removeRole(ApplicationRoleEnum|string $role): void
    {
        $role = is_string($role) ? ApplicationRoleEnum::from($role) : $role;

        $key = array_search($role->value, $this->roles, true);
        if (false !== $key) {
            unset($this->roles[$key]);
        }
    }

    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): void
    {
        if (!$this->posts->contains($post)) {
            $this->posts->add($post);
            $post->setApplication($this);
        }
    }

    public function removePost(Post $post): void
    {
        $this->posts->removeElement($post);
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): void
    {
        $this->user = $user;
    }
}
