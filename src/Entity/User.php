<?php
declare(strict_types=1);

namespace App\Entity;

use App\Enum\UserRoleEnum;
use App\Repository\UserRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`users`')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\Column(name: "id", type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?Uuid $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    /** @var ?string The hashed password */
    #[ORM\Column]
    private ?string $password = null;

    /** @var string[] */
    #[ORM\Column(name: "roles", type: "text[]", nullable: false)]
    private array $roles = [ UserRoleEnum::DEFAULT ];

    #[ORM\Column(name: "created_at", type: Types::DATETIMETZ_IMMUTABLE, nullable: false)]
    private \DateTimeImmutable $createdAt;

    #[ORM\Column(name: "updated_at", type: Types::DATETIMETZ_IMMUTABLE, nullable: false)]
    private \DateTimeImmutable $updatedAt;

    #[ORM\Column(name: "deleted_at", type: Types::DATETIMETZ_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Application::class, cascade: ['remove'], orphanRemoval: true)]
    private Collection $applications;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();

        $this->applications = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getUserIdentifier(): string
    {
        return (string)$this->email;
    }

    /** @return string[] */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /** @throws \ValueError */
    public function addRole(UserRoleEnum|string $role): void
    {
        $role = is_string($role) ? UserRoleEnum::from($role) : $role;

        $this->roles[] = $role->value;
        $this->roles = array_unique($this->roles);
    }

    /** @throws \ValueError */
    public function removeRole(UserRoleEnum|string $role): void
    {
        $role = is_string($role) ? UserRoleEnum::from($role) : $role;

        $key = array_search($role->value, $this->roles, true);
        if (false !== $key) {
            unset($this->roles[$key]);
        }
    }

    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function getApplications(): Collection
    {
        return $this->applications;
    }

    public function addApplication(Application $application): void
    {
        if (!$this->applications->contains($application)) {
            $this->applications->add($application);
            $application->setUser($this);
        }
    }

    public function removeApplication(Application $application): void
    {
        $this->applications->removeElement($application);
    }

    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
