<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken as BaseRefreshToken;

#[ORM\Table(name: "refresh_tokens")]
#[ORM\Entity]
class RefreshToken extends BaseRefreshToken
{
//    #[ORM\Id]
//    #[ORM\Column(name:"id", type:Types::INTEGER, nullable:false)]
    #[ORM\GeneratedValue(strategy:"SEQUENCE")]
    #[ORM\SequenceGenerator(sequenceName:"refresh_tokens_id_seq", allocationSize:1, initialValue:1)]
    protected $id;

//    #[ORM\Column(name: "valid", type: Types::DATETIMETZ_IMMUTABLE, nullable: false)]
    protected $valid;
}
