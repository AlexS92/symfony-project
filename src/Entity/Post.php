<?php
declare(strict_types=1);

namespace App\Entity;

use App\Enum\PostStatusEnum;
use Doctrine\DBAL\Types\Types;
use App\Doctrine\ComputeSlugInterface;
use App\Repository\PostRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Uid\Uuid;

#[ORM\Table(
    name: "posts",
    uniqueConstraints: [
        new ORM\UniqueConstraint(name: "posts_slug_application_uniq", columns: ["slug", "application_id"])
    ]
)]
#[ORM\Entity(repositoryClass: PostRepository::class)]
class Post implements ComputeSlugInterface
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?Uuid $id = null;

    #[ORM\Column(name: "title", type: Types::STRING, length: 255, nullable: false)]
    private string $title;

    #[ORM\Column(name: "slug", type: Types::STRING, nullable: false)]
    private string $slug = '';

    /** @var string[] */
    #[ORM\Column(name: "tags", type: "text[]", nullable: false)]
    private array $tags = [];

    #[ORM\Column(name: "created_at", type: Types::DATETIMETZ_IMMUTABLE, nullable: false)]
    private \DateTimeImmutable $createdAt;

    #[ORM\Column(name: "updated_at", type: Types::DATETIMETZ_IMMUTABLE, nullable: false)]
    private \DateTimeImmutable $updatedAt;

    #[ORM\Column(name: "deleted_at", type: Types::DATETIMETZ_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    #[ORM\Column(name: "status", type: Types::STRING, nullable: false, enumType: PostStatusEnum::class)]
    private PostStatusEnum $status;

    #[ORM\ManyToOne(targetEntity: Application::class, inversedBy: 'posts')]
    #[ORM\JoinColumn(name: "application_id", referencedColumnName: "id", nullable: false)]
    private ?Application $application;

    public function __construct(string $title, ?PostStatusEnum $status = null)
    {
        $this->title = $title;
        $this->status = $status ?? PostStatusEnum::from(PostStatusEnum::DEFAULT);

        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
        $this->slug = ''; //for slug generation
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /** @return string[] */
    public function getTags(): array
    {
        return $this->tags;
    }

    /** @param string[] $tags */
    public function setTags(array $tags): void
    {
        $this->tags = array_unique($tags);
    }

    public function getApplication(): ?Application
    {
        return $this->application;
    }

    public function setApplication(Application $application): void
    {
        $this->application = $application;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function getStatus(): PostStatusEnum
    {
        return $this->status;
    }

    public function setStatus(PostStatusEnum $status): void
    {
        $this->status = $status;
    }

    #[\Override]
    public function computeSlug(SluggerInterface $slugger): void
    {
        if ('' === $this->slug) {
            $this->slug = (string)$slugger->slug($this->title)->lower();
        }
    }
}
