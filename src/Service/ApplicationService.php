<?php
declare(strict_types=1);

namespace App\Service;

use App\Api\Request\CreateApplicationDto;
use App\Entity\Application;
use App\Entity\User;
use App\Exception\DomainException;
use App\Repository\ApplicationRepository;
use App\Service\Shared\JwtService;

readonly class ApplicationService
{
    private JwtService $jwtService;
    private ApplicationRepository $applicationRepository;

    public function __construct(
        JwtService $jwtService,
        ApplicationRepository $applicationRepository,
    ) {
        $this->jwtService = $jwtService;
        $this->applicationRepository = $applicationRepository;
    }

    /** @throws DomainException */
    public function createApplication(User $user, CreateApplicationDto $createApplicationDto): Application
    {
        $application = new Application($createApplicationDto->getName());
        $application->setStatus($createApplicationDto->getStatus());
        $application->setDescription($createApplicationDto->getDescription());
        $application->setUser($user);

        try {
            $this->applicationRepository->save($application);
        } catch (\Throwable) {
            throw new DomainException();
        }

        return $application;
    }

    /** @throws DomainException */
    public function generateApplicationToken(Application $application): Application
    {
        try {
            $token = $this->jwtService->generateToken()->toString();
            $application->setToken($token);

            $this->applicationRepository->save($application);
        } catch (\Throwable) {
            throw new DomainException();
        }

        return $application;
    }
}
