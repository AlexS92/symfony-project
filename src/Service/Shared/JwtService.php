<?php
declare(strict_types=1);

namespace App\Service\Shared;

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\Validation\Constraint\SignedWith;

readonly class JwtService
{
    private Configuration $configuration;

    /** @phpstan-param non-empty-string $appJwtKey */
    public function __construct(string $appJwtKey)
    {
        assert(strlen($appJwtKey) * 8 >= 256, new \AssertionError('Invalid length of JWT key'));

        $configuration = Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText($appJwtKey)
        );

        $configuration->setValidationConstraints(
            new SignedWith($configuration->signer(), $configuration->verificationKey())
        );

        $this->configuration = $configuration;
    }

    public function generateToken(): Token
    {
        $builder = $this->configuration
            ->builder()
            ->issuedAt(new \DateTimeImmutable())
        ;

        return $builder->getToken(
            $this->configuration->signer(),
            $this->configuration->signingKey()
        );
    }

    public function isTokenVerified(Token $token): bool
    {
        $constraints = $this->configuration->validationConstraints();

        return $this->configuration->validator()->validate($token, ...$constraints);
    }

    /** @phpstan-param non-empty-string $token */
    public function parseToken(string $token): Token
    {
        return $this->configuration->parser()->parse($token);
    }

    public function isTokenValid(string $jwtString): bool
    {
        assert(!empty($jwtString), new \AssertionError('Empty token'));

        try {
            $token = $this->parseToken($jwtString);
        } catch (\Throwable) {
            return false;
        }

        return $this->isTokenVerified($token);
    }
}
