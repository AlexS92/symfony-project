<?php
declare(strict_types=1);

namespace App\Service\Shared;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Uuid;

readonly class EntityUuidGenerator
{
    /** @var EntityManager  */
    private EntityManagerInterface $entityManager;
    private UuidGenerator $uuidGenerator;

    /** @param EntityManager $entityManager */
    public function __construct(EntityManagerInterface $entityManager, UuidGenerator $uuidGenerator)
    {
        $this->entityManager = $entityManager;
        $this->uuidGenerator = $uuidGenerator;
    }

    public function generateUuidForEntity(object $entity): void
    {
        $reflection = new \ReflectionClass($entity);

        assert($this->isObjectEntity($reflection), new \AssertionError('Invalid object passed to generateUuidForEntity.'));
        assert($reflection->hasProperty('id'), new \AssertionError('Passed entity doesn\'t have an id property.'));

        $idProperty = $reflection->getProperty('id');

        assert($idProperty->getType() instanceof \ReflectionNamedType, new \AssertionError('The passed entity id property has an incompatible property type.'));
        assert(Uuid::class === $idProperty->getType()->getName(), new \AssertionError('Type of id property of entity is not UUID.'));

        $uuid = $this->uuidGenerator->generate($this->entityManager, $entity);
        $idProperty->setValue($entity, $uuid);
    }

    private function isObjectEntity(\ReflectionClass $object): bool
    {
        $attributes = $object->getAttributes();
        foreach ($attributes as $attribute) {
            if (Entity::class === $attribute->getName()) {
                return true;
            }
        }

        return false;
    }
}
