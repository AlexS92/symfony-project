<?php
declare(strict_types=1);

namespace App\Service\Shared;

use Psr\Cache\CacheException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\RedisTagAwareAdapter;
use Symfony\Component\Cache\CacheItem;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

readonly class CacheService
{
    /** @var RedisTagAwareAdapter */
    private TagAwareCacheInterface $adapter;

    /** @param RedisTagAwareAdapter $adapter */
    public function __construct(TagAwareCacheInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /** @throws InvalidArgumentException */
    public function get(string $key, mixed $default = null): ?CacheItem
    {
        if ($this->adapter->hasItem($key)) {
            /** @var CacheItem $item */
            $item = $this->adapter->getItem($key);
            if ($item->isHit()) {
                return $item;
            }
        }

        return $default;
    }

    /**
     * @param string[]  $tags
     *
     * @throws CacheException
     * @throws InvalidArgumentException
     */
    public function set(string $key, mixed $value, ?\DateTimeInterface $expiration = null, array $tags = []): bool
    {
        /** @var CacheItem $item */
        $item = $this->adapter->getItem($key);
        $item->set($value)->tag($tags)->expiresAt($expiration);

        return $this->adapter->save($item);
    }

    /** @throws InvalidArgumentException */
    public function delete(string $key): bool
    {
        return $this->adapter->deleteItem($key);
    }

    /**
     * @param string[] $tags
     *
     * @throws InvalidArgumentException
     */
    public function invalidateTags(array $tags): bool
    {
        return $this->adapter->invalidateTags($tags);
    }
}
