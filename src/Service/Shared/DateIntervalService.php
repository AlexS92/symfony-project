<?php
declare(strict_types=1);

namespace App\Service\Shared;

readonly class DateIntervalService
{
    public function createDateIntervalFromString(string $interval): ?\DateInterval
    {
        $dateInterval = \DateInterval::createFromDateString($interval);
        if ($dateInterval instanceof \DateInterval) {
            return $dateInterval;
        }

        try {
            return new \DateInterval($interval);
        } catch (\Throwable) {
        }

        return null;
    }
}
