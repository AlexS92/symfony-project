<?php
declare(strict_types=1);

namespace App\Service\Shared;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

readonly class PhoneUtilService
{
    private PhoneNumberUtil $phoneUtil;

    public function __construct()
    {
        $this->phoneUtil = PhoneNumberUtil::getInstance();
    }

    /** @throws NumberParseException */
    public function parseToPhoneNumber(string $phoneNumber, ?string $defaultRegionCode = null): PhoneNumber
    {
        return $this->phoneUtil->parse($phoneNumber, $defaultRegionCode);
    }

    public function isValidNumber(PhoneNumber $phoneNumber): bool
    {
        return $this->phoneUtil->isValidNumber($phoneNumber);
    }

    public function isValidNumberForRegion(PhoneNumber $phoneNumber, string $regionCode): bool
    {
        return $this->phoneUtil->isValidNumberForRegion($phoneNumber, $regionCode);
    }

    public function format(PhoneNumber $phoneNumber, int $numberFormat = PhoneNumberFormat::INTERNATIONAL): string
    {
        return $this->phoneUtil->format($phoneNumber, $numberFormat);
    }

    public function formatToNumericPhoneNumber(PhoneNumber $phoneNumber): string
    {
        $internationalPhoneNumber = $this->phoneUtil->format($phoneNumber, PhoneNumberFormat::INTERNATIONAL);

        /** @var string[] $parts */
        $parts = preg_split('~\D++~', $internationalPhoneNumber, -1, PREG_SPLIT_NO_EMPTY);

        return implode('', $parts);
    }
}
