<?php
declare(strict_types=1);

namespace App\Service;

use App\Api\Request\CreatePostDto;
use App\Api\Request\CreatePostDtoCollection;
use App\Api\Request\EditPostDto;
use App\Entity\Application;
use App\Entity\Post;
use App\Exception\DomainException;
use App\Repository\PostRepository;
use App\Service\Shared\EntityUuidGenerator;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

readonly class PostService
{
    private PostRepository $postRepository;
    private NormalizerInterface $snakeCaseNormalizer;
    private SluggerInterface $slugger;
    private EntityUuidGenerator $entityUuidGenerator;
    private LoggerInterface $logger;

    public function __construct(
        PostRepository $postRepository,
        NormalizerInterface $snakeCaseNormalizer,
        SluggerInterface $slugger,
        EntityUuidGenerator $entityUuidGenerator,
        LoggerInterface $logger
    ) {
        $this->postRepository = $postRepository;
        $this->snakeCaseNormalizer = $snakeCaseNormalizer;
        $this->slugger = $slugger;
        $this->entityUuidGenerator = $entityUuidGenerator;
        $this->logger = $logger;
    }


    /** @return Post[] */
    public function findPostsByApplication(Application $application): array
    {
        return $this->postRepository->findBy(['application' => $application]);
    }

    /** @throws DomainException */
    public function editPostByDto(Post $post, EditPostDto $editPostDto): Post
    {
        try {
            $post->setTags($editPostDto->getTags());
            $this->postRepository->save($post);

            return $post;
        } catch (\Throwable) {
            throw new DomainException();
        }
    }

    /**
     * @return Post[]
     *
     * @throws DomainException
     */
    public function multiCreatePost(CreatePostDtoCollection $createPostDtoCollection, Application $application): array
    {
        $entityList = [];
        foreach ($createPostDtoCollection as $createPostDto) {
            $title = $createPostDto->getTitle();

            //отслеживать необходимо самому т.к. DoctrineEntityListener не вызывается
            $slug = (string)$this->slugger->slug($title)->lower();

            $post = new Post($title);
            $post->setTags($createPostDto->getTags());
            $post->setSlug($slug);

            $this->entityUuidGenerator->generateUuidForEntity($post);

            $entityList[] = $post;
        }

        try {
            $entityMapList = [];
            foreach ($entityList as $post) {
                /** @var array<string, mixed> $normalizedEntity */
                $normalizedEntity = $this->snakeCaseNormalizer->normalize($post, null, [
                    AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
                    AbstractNormalizer::IGNORED_ATTRIBUTES => ['application']
                ]);

                $entityMapList[] = $normalizedEntity;
            }

            $entityList = [];
            if (!empty($entityMapList)) {
                $entityList = $this->postRepository->insertOrUpdate($entityMapList, $application);
            }

            return $entityList;
        } catch (\Throwable $exception) {
            $this->logger->error('Error while `multiCreatePost`', [
                'message' => $exception->getMessage()
            ]);

            throw new DomainException();
        }
    }

    /** @throws DomainException */
    public function createPostByDto(Application $application, CreatePostDto $createPostDto): Post
    {
        $title = $createPostDto->getTitle();
        try {
            $post = new Post($title);
            $post->setApplication($application);
            $post->setTags($createPostDto->getTags());

            $this->postRepository->save($post);

            return $post;
        } catch (UniqueConstraintViolationException) {
            throw new DomainException("Post with title: `{$title}` already exists.", Response::HTTP_CONFLICT);
        } catch (\Throwable) {
            throw new DomainException();
        }
    }
}
