<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use App\Service\Shared\PhoneUtilService;
use libphonenumber\NumberParseException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class PhoneNumberValidator extends ConstraintValidator
{
    private readonly PhoneUtilService $phoneUtil;

    public function __construct(PhoneUtilService $phoneUtil)
    {
        $this->phoneUtil = $phoneUtil;
    }

    #[\Override]
    public function validate(mixed $value, Constraint $constraint): void
    {
        $value = (string)$value;
        if ('' === $value) {
            return;
        }

        if (!($constraint instanceof PhoneNumber)) {
            throw new UnexpectedTypeException($constraint, PhoneNumber::class);
        }

        try {
            $phoneNumber = $this->phoneUtil->parseToPhoneNumber($value, $constraint->getRegionCode());
        } catch (NumberParseException) {
            $this->context->buildViolation($constraint->getParsingErrorMessage())
                ->setParameter('{{phone}}', $value)
                ->addViolation();

            return;
        }

        if ($constraint->isValidationNeeded()) {
            $valid = match ($constraint->getRegionCode()) {
                null => $this->phoneUtil->isValidNumber($phoneNumber),
                default => $this->phoneUtil->isValidNumberForRegion($phoneNumber, $constraint->getRegionCode()),
            };

            if (!$valid) {
                $this->context->buildViolation($constraint->getValidationErrorMessage())
                    ->setParameter('{{phone}}', $value)
                    ->addViolation();
            }
        }
    }
}
