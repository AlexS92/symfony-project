<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class EntityExistValidator extends ConstraintValidator
{
    private readonly EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[\Override]
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!($constraint instanceof EntityExist)) {
            throw new UnexpectedTypeException($constraint, EntityExist::class);
        }

        $value = (string)$value;
        if ('' === $value) {
            return;
        }

        try {
            $repository = $this->entityManager->getRepository($constraint->getEntityClass());

            $criteria = [$constraint->getField() => $value] + $constraint->getAdditionalParams();
            $entity = $repository->findOneBy($criteria);

            if ($constraint->isExistenceRequired() xor null !== $entity) {
                $this->buildConstraintViolation($value, $constraint);
            }
        } catch (\Throwable) {
            $this->buildConstraintViolation($value, $constraint);
        }
    }

    private function buildConstraintViolation(string $value, EntityExist $constraint): void
    {
        $message = $constraint->getMessageObjectMustNotExist();
        if ($constraint->isExistenceRequired()) {
            $message = $constraint->getMessageObjectMustExist();
        }

        $this->context->buildViolation($message)
            ->setParameter('{{class}}', $constraint->getEntityClass())
            ->setParameter('{{field}}', $constraint->getField())
            ->setParameter('{{value}}', $value)
            ->addViolation();
    }
}
