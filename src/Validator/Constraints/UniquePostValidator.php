<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use App\Repository\PostRepository;
use App\Security\ApplicationSecurityDto\ApplicationToken;
use App\Security\ApplicationSecurityDto\ApplicationAsUser;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UniquePostValidator extends ConstraintValidator
{
    private readonly TokenStorageInterface $tokenStorage;
    private readonly PostRepository $postRepository;
    private readonly SluggerInterface $slugger;

    public function __construct(TokenStorageInterface $tokenStorage, PostRepository $postRepository, SluggerInterface $slugger)
    {
        $this->tokenStorage = $tokenStorage;
        $this->postRepository = $postRepository;
        $this->slugger = $slugger;
    }

    #[\Override]
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!($constraint instanceof UniquePost)) {
            throw new UnexpectedTypeException($constraint, UniquePost::class);
        }

        $applicationUser = $this->tokenStorage->getToken()?->getUser();
        if (!$applicationUser instanceof ApplicationAsUser) {
            throw new BadCredentialsException('ApplicationBadge token check failed');
        }

        $slug = (string)$this->slugger->slug($value)->lower();
        $post = $this->postRepository->findOneBy([
            'application' => $applicationUser->getApplication(),
            'slug' => $slug,
        ]);

        if ($post !== null) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{title}}', $value)
                ->addViolation();
        }
    }
}
