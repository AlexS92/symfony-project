<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use Doctrine\Common\Annotations\Annotation\Target;
use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class UniquePost extends Constraint
{
    public string $message = 'The post with the title: `{{title}}` already exists.';

    #[\Override]
    public function validatedBy(): string
    {
        return UniquePostValidator::class;
    }
}
