<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use Ramsey\Collection\CollectionInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class CollectionValidValidator extends ConstraintValidator
{
    /** @param CollectionValid $constraint */
    #[\Override]
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof CollectionValid) {
            throw new UnexpectedTypeException($constraint, CollectionValid::class);
        }

        if (!($value instanceof CollectionInterface)) {
            return;
        }

        $count = $value->count();
        if (null !== $constraint->getMin() && $count < $constraint->getMin()) {
            $this->context->buildViolation($constraint->getMinErrorMessage())
                ->setParameter('{{min}}', (string)$constraint->getMin())
                ->setParameter('{{count}}', (string)$count)
                ->setInvalidValue(null)
                ->addViolation();
            return;
        }

        if (null !== $constraint->getMax() && $count > $constraint->getMax()) {
            $this->context->buildViolation($constraint->getMaxErrorMessage())
                ->setParameter('{{max}}', (string)$constraint->getMax())
                ->setParameter('{{count}}', (string)$count)
                ->setInvalidValue(null)
                ->addViolation();
            return;
        }

        $validator = $this->context
            ->getValidator()
            ->inContext($this->context);

        foreach ($value as $element) {
            $validator->validate(
                value: $element,
                groups: $this->context->getGroup()
            );
        }
    }
}
