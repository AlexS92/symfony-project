<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class EntityExist extends Constraint
{
    private readonly string $field;
    private readonly bool $isExistenceRequired;

    /** @var array<string, string|int|bool|double> */
    private readonly array $additionalParams;

    private readonly string $entityClass;

    private string $messageObjectMustExist = 'Entity with class: {{class}} was not discovered by field: {{field}} and value: {{value}}.';
    private string $messageObjectMustNotExist = 'Entity with class: {{class}} was discovered by field: {{field}} and value: {{value}}.';

    /**
     * @param array<string, string|int|bool|double> $additionalParams
     *
     * @inheritDoc
     */
    public function __construct(
        string $entityClass,
        string $field = 'id',
        bool $isExistenceRequired = true,
        array $additionalParams = [],
        mixed $options = null,
        ?array $groups = null,
        mixed $payload = null
    ) {
        $this->entityClass = $entityClass;
        $this->field = $field;
        $this->isExistenceRequired = $isExistenceRequired;
        $this->additionalParams = $additionalParams;

        parent::__construct($options, $groups, $payload);
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function isExistenceRequired(): bool
    {
        return $this->isExistenceRequired;
    }

    /** @return array<string, string|int|bool|double> */
    public function getAdditionalParams(): array
    {
        return $this->additionalParams;
    }

    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    public function getMessageObjectMustExist(): string
    {
        return $this->messageObjectMustExist;
    }

    public function getMessageObjectMustNotExist(): string
    {
        return $this->messageObjectMustNotExist;
    }

    #[\Override]
    public function validatedBy(): string
    {
        return EntityExistValidator::class;
    }
}
