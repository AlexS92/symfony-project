<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class PhoneNumber extends Constraint
{
    private readonly ?string $regionCode;
    private readonly bool $validationNeeded;

    private string $parsingErrorMessage = "The phone number is not parsable: {{phone}}.";
    private string $validationErrorMessage = "The phone number is not valid: {{phone}}.";

    public function __construct(
        ?string $regionCode = null,
        bool $validationNeeded = true,
        mixed $options = null,
        ?array $groups = null,
        mixed $payload = null
    ) {
        $this->regionCode = $regionCode;
        $this->validationNeeded = $validationNeeded;

        parent::__construct($options, $groups, $payload);
    }

    public function getRegionCode(): ?string
    {
        return $this->regionCode;
    }

    public function isValidationNeeded(): bool
    {
        return $this->validationNeeded;
    }

    public function getParsingErrorMessage(): string
    {
        return $this->parsingErrorMessage;
    }

    public function getValidationErrorMessage(): string
    {
        return $this->validationErrorMessage;
    }

    #[\Override]
    public function validatedBy(): string
    {
        return PhoneNumberValidator::class;
    }
}
