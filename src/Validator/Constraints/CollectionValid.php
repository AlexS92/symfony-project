<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY)]
class CollectionValid extends Constraint
{
    private readonly ?int $max;
    private readonly ?int $min;

    private string $minErrorMessage = "The minimum collection element count is {{min}}. Given: {{count}}.";
    private string $maxErrorMessage = "The maximum collection element count is {{max}}. Given: {{count}}.";

    public function __construct(
        ?int $max = null,
        ?int $min = null,
        mixed $options = null,
        ?array $groups = null,
        mixed $payload = null
    ) {
        $this->max = $max;
        $this->min = $min;

        parent::__construct($options, $groups, $payload);
    }

    #[\Override]
    public function validatedBy(): string
    {
        return CollectionValidValidator::class;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function getMinErrorMessage(): string
    {
        return $this->minErrorMessage;
    }

    public function getMaxErrorMessage(): string
    {
        return $this->maxErrorMessage;
    }
}
