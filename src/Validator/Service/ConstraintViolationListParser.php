<?php
declare(strict_types=1);

namespace App\Validator\Service;

use App\Api\Response\Error\ApiError;
use Symfony\Component\Validator\ConstraintViolationListInterface;

readonly class ConstraintViolationListParser
{
    /** @return ApiError[] */
    public function parseToApiErrorList(ConstraintViolationListInterface $list): array
    {
        $errors = [];
        foreach ($list as $item) {
            $errors[] = new ApiError(
                (string) $item->getMessage(),
                $item->getPropertyPath(),
                $item->getInvalidValue()
            );
        }

        return $errors;
    }
}
