<?php
declare(strict_types=1);

namespace App\Validator\Service;

use Symfony\Component\Validator\ConstraintViolation;

readonly class ConstraintViolationManager
{
    public function setPathAndValueToConstraintViolation(ConstraintViolation $constraintViolation, ?string $path = null, mixed $value = null): void
    {
        $setPathAndValue = \Closure::bind(function ($constraintViolation, $path, $value) {
            $constraintViolation->propertyPath = $path;
            $constraintViolation->invalidValue = $value;
        }, null, ConstraintViolation::class);

        $setPathAndValue($constraintViolation, $path, $value);
    }
}
