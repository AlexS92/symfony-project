<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class HttpApiValidationException extends \Exception
{
    private readonly ?ConstraintViolationListInterface $constraintViolationList;

    /** @var string[] */
    private readonly array $context;

    /** @param string[] $context */
    public function __construct(string $message = "Validation Failed", int $code = 400, \Throwable $previous = null, ?ConstraintViolationListInterface $constraintViolationList = null, array $context = [])
    {
        $this->constraintViolationList = $constraintViolationList;
        $this->context = $context;

        parent::__construct($message, $code, $previous);
    }

    public function getConstraintViolationList(): ?ConstraintViolationListInterface
    {
        return $this->constraintViolationList;
    }

    /** @return string[] */
    public function getContext(): array
    {
        return $this->context;
    }
}
