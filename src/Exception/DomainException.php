<?php
declare(strict_types=1);

namespace App\Exception;

class DomainException extends \Exception
{
    /** @var string[] */
    private readonly array $context;

    /** @param string[] $context */
    public function __construct(string $message = "Something went wrong, please try again later.", int $code = 0, \Throwable $previous = null, array $context = [])
    {
        $this->context = $context;

        parent::__construct($message, $code, $previous);
    }

    /** @return string[] */
    public function getContext(): array
    {
        return $this->context;
    }
}
