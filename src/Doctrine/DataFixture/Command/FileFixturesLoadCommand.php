<?php
declare(strict_types=1);

namespace App\Doctrine\DataFixture\Command;

use App\Doctrine\DataFixture\Service\FileFixtureLoader;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

#[AsCommand(name: 'file:fixtures:load', description: 'Load data fixtures to the database.')]
class FileFixturesLoadCommand extends Command
{
    private readonly FileFixtureLoader $loader;

    public function __construct(FileFixtureLoader $loader)
    {
        $this->loader = $loader;

        parent::__construct();
    }

    #[\Override]
    protected function configure(): void
    {
        $this
            ->setDescription('Load data fixtures to the database.')
            ->addOption(
                'append',
                null,
                InputOption::VALUE_NONE,
                'Append the data fixtures instead of deleting all data from the database first.'
            )
            ->addOption(
                'purge-with-truncate',
                null,
                InputOption::VALUE_NONE,
                'Purge data by using a database-level TRUNCATE statement when using Doctrine fixtures.'
            )
            ->addOption(
                'folder',
                'f',
                InputOption::VALUE_OPTIONAL,
                'Path to directory in environment.',
                ''
            )
            ->addArgument(
                'filename-patterns',
                InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                'Patterns of filename which will be loaded.',
                ['/.*\.(ya?ml|php)$/i']
            )
        ;
    }

    #[\Override]
    public function setApplication(ConsoleApplication $application = null): void
    {
        if (null !== $application && false === $application instanceof Application) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Expected application to be an instance of "%s".',
                    Application::class
                )
            );
        }

        parent::setApplication($application);
    }

    #[\Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($input->isInteractive() && !$input->getOption('append')) {
            if (!$this->askConfirmation($input, $output)) {
                return self::SUCCESS;
            }
        }

        /** @var Application $application */
        $application = $this->getApplication();
        $folder = $input->getOption('folder');
        $environment = $input->getOption('env');
        $append = $input->getOption('append');
        $truncate = $input->getOption('purge-with-truncate');
        $filenamePatterns = $input->getArgument('filename-patterns');

        $this->loader->load($application, $environment, $folder, $append, $truncate, $filenamePatterns);

        return self::SUCCESS;
    }

    private function askConfirmation(InputInterface $input, OutputInterface $output): bool
    {
        /** @var QuestionHelper $questionHelper */
        /** @phpstan-ignore-next-line */
        $questionHelper = $this->getHelperSet()->get('question');
        $question = new ConfirmationQuestion('<question>Careful, database will be purged. Do you want to continue y/N ?</question>', false);

        return (bool)$questionHelper->ask($input, $output, $question);
    }
}
