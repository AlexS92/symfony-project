<?php
declare(strict_types=1);

namespace App\Doctrine\DataFixture\Service;

use Doctrine\ORM\EntityManagerInterface;
use Fidry\AliceDataFixtures\Bridge\Doctrine\Persister\ObjectManagerPersister;
use Fidry\AliceDataFixtures\LoaderInterface;
use Fidry\AliceDataFixtures\Persistence\PersisterAwareInterface;
use Fidry\AliceDataFixtures\Persistence\PurgeMode;
use Symfony\Bundle\FrameworkBundle\Console\Application;

readonly class FileFixtureLoader
{
    private FileFixtureLocator $fixtureLocator;
    private PersisterAwareInterface $purgeLoader;
    private PersisterAwareInterface $appendLoader;
    private EntityManagerInterface $entityManager;

    public function __construct(
        FileFixtureLocator $fixtureLocator,
        PersisterAwareInterface $purgeLoader,
        PersisterAwareInterface $appendLoader,
        EntityManagerInterface $entityManager
    ) {
        $this->fixtureLocator = $fixtureLocator;
        $this->purgeLoader = $purgeLoader;
        $this->appendLoader = $appendLoader;
        $this->entityManager = $entityManager;
    }

    /**
     * @param string[] $filenamePatterns
     * @return object[]
     */
    public function load(
        Application $application,
        string      $environment,
        string      $folder,
        bool        $append,
        bool        $purgeWithTruncate,
        array       $filenamePatterns
    ): array {
        if ($append && $purgeWithTruncate) {
            throw new \LogicException(
                'Cannot append loaded fixtures and at the same time purge the database. Choose one.'
            );
        }

        $fixtureFiles = $this->fixtureLocator->locateFixtureFiles($environment, $folder, $filenamePatterns);

        $purgeMode = $this->retrievePurgeMode($append, $purgeWithTruncate);

        return $this->loadFixtures(
            $append ? $this->appendLoader : $this->purgeLoader,
            $this->entityManager,
            $fixtureFiles,
            /** @phpstan-ignore-next-line */
            $application->getKernel()->getContainer()->getParameterBag()->all(),
            $purgeMode
        );
    }

    /**
     * @param string[] $files
     * @param array<mixed> $parameters
     *
     * @return object[]
     */
    protected function loadFixtures(
        PersisterAwareInterface $loader,
        EntityManagerInterface $manager,
        array $files,
        array $parameters,
        ?PurgeMode $purgeMode
    ): array {
        $persister = new ObjectManagerPersister($manager);

        /** @var LoaderInterface $loader */
        $loader = $loader->withPersister($persister);

        return $loader->load($files, $parameters, [], $purgeMode);
    }

    private function retrievePurgeMode(bool $append, bool $purgeWithTruncate): ?PurgeMode
    {
        if ($append) {
            return null;
        }

        return (true === $purgeWithTruncate) ? PurgeMode::createTruncateMode() : PurgeMode::createDeleteMode();
    }
}
