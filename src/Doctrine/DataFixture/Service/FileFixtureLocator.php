<?php
declare(strict_types=1);

namespace App\Doctrine\DataFixture\Service;

use Symfony\Component\Finder\Finder as SymfonyFinder;

readonly class FileFixtureLocator
{
    /** @var string[] */
    private array $fixturePaths;
    /** @var string[] */
    private array $rootDirs;

    /**
     * @param string[] $fixturePaths
     * @param string[] $rootDirs
     */
    public function __construct(array $fixturePaths, array $rootDirs)
    {
        $this->fixturePaths = $fixturePaths;
        $this->rootDirs = $rootDirs;
    }

    /**
     * @param string[] $filenamePatterns
     *
     * @return string[]
     */
    public function locateFixtureFiles(string $environment, string $folder, array $filenamePatterns): array
    {
        $fixtureFiles = [];
        foreach ($this->rootDirs as $rootDir) {
            $fixtureFiles[] = $this->locateFiles($rootDir, $environment, $folder, $filenamePatterns);
            $fixtureFiles[] = $this->locateFiles($rootDir, $environment, '', ['/.*\.(ya?ml|php)$/i']);
            $fixtureFiles[] = $this->locateFiles($rootDir, '', '', ['/.*\.(ya?ml|php)$/i']);
        }

        return array_unique(array_merge(...$fixtureFiles));
    }

    /**
     * @param string[] $filenamePatterns
     *
     * @return string[]
     */
    private function locateFiles(string $rootDir, string $environment, string $folder, array $filenamePatterns): array
    {
        $filePaths = [];
        foreach ($this->fixturePaths as $fixturePath) {
            $filePaths[] = realpath(sprintf('%s/%s/%s/%s', $rootDir, $fixturePath, $environment, $folder));
        }

        $fullPaths = array_filter($filePaths, fn($fullPath) => $fullPath && file_exists($fullPath));
        if ([] === $fullPaths) {
            return [];
        }

        $files = SymfonyFinder::create()->files()->in($fullPaths)->depth(0)->name($filenamePatterns);

        // this sort helps to set an order with filename
        $files = $files->sort(fn(\SplFileInfo $a, \SplFileInfo $b) => strcasecmp($a->getFilename(), $b->getFilename()));

        $fixtureFiles = [];
        foreach ($files as $file) {
            $fixtureFiles[(string) $file->getRealPath()] = true;
        }

        return array_keys($fixtureFiles);
    }
}
