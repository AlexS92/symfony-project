<?php
declare(strict_types=1);

namespace App\Doctrine\DataFixture;

use Doctrine\Common\DataFixtures\Purger\ORMPurger as DoctrineOrmPurger;
use Doctrine\Common\DataFixtures\Purger\PurgerInterface as DoctrinePurgerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Fidry\AliceDataFixtures\Persistence\PurgeMode;
use Fidry\AliceDataFixtures\Persistence\PurgerFactoryInterface;
use Fidry\AliceDataFixtures\Persistence\PurgerInterface;
use InvalidArgumentException;
use Nelmio\Alice\IsAServiceTrait;

class Purger implements PurgerInterface, PurgerFactoryInterface
{
    use IsAServiceTrait;

    private ObjectManager $manager;
    private DoctrinePurgerInterface $purger;

    /** @var string[] */
    private array $excludedTables;

    /** @var string[] */
    private array $restartSequence;

    /**
     * @param string[] $excludedTables
     * @param string[] $restartSequence
     */
    public function __construct(
        ObjectManager $manager,
        array $excludedTables = [],
        array $restartSequence = [],
        PurgeMode $purgeMode = null
    ) {
        $this->manager = $manager;
        $this->excludedTables = $excludedTables;
        $this->restartSequence = $restartSequence;

        $this->purger = self::createPurger($manager, $purgeMode, $excludedTables);
    }

    /** @inheritdoc */
    #[\Override]
    public function create(PurgeMode $mode, PurgerInterface $purger = null): PurgerInterface
    {
        if (null === $purger) {
            return new self(
                $this->manager,
                $this->excludedTables,
                $this->restartSequence,
                $mode
            );
        }

        if ($purger instanceof DoctrinePurgerInterface) {
            /** @phpstan-ignore-next-line */
            $manager = $purger->getObjectManager();
        } elseif ($purger instanceof self) {
            $manager = $purger->manager;
        } else {
            throw new InvalidArgumentException(
                sprintf(
                    'Expected purger to be either and instance of "%s" or "%s". Got "%s".',
                    DoctrinePurgerInterface::class,
                    self::class,
                    $purger::class
                )
            );
        }

        if (null === $manager) {
            throw new InvalidArgumentException(
                sprintf(
                    'Expected purger "%s" to have an object manager, got "null" instead.',
                    $purger::class
                )
            );
        }

        return new self(
            $this->manager,
            $this->excludedTables,
            $this->restartSequence,
            $mode
        );
    }

    /**
     * @inheritdoc
     */
    #[\Override]
    public function purge(): void
    {
        $this->purger->purge();

        /** @phpstan-ignore-next-line */
        $connection = $this->purger->getObjectManager()->getConnection();
        foreach ($this->restartSequence as $sequence) {
            $connection->exec(sprintf("ALTER SEQUENCE %s RESTART", $sequence));
        }
    }

    /** @param string[] $excludedTables */
    private static function createPurger(
        ObjectManager $manager,
        ?PurgeMode $purgeMode,
        array $excludedTables = []
    ): DoctrinePurgerInterface {
        if ($manager instanceof EntityManagerInterface) {
            $purger = new DoctrineOrmPurger($manager, $excludedTables);

            if (null !== $purgeMode) {
                $purger->setPurgeMode($purgeMode->getValue());
            }

            return $purger;
        }

        throw new InvalidArgumentException(
            sprintf(
                'Cannot create a purger for ObjectManager of class %s',
                $manager::class
            )
        );
    }
}
