<?php
declare(strict_types=1);

namespace App\Doctrine;

use Symfony\Component\String\Slugger\SluggerInterface;

interface ComputeSlugInterface
{
    public function computeSlug(SluggerInterface $slugger): void;
}
