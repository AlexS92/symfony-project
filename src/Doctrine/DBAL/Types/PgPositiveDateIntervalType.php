<?php
declare(strict_types=1);

namespace App\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

class PgPositiveDateIntervalType extends Type
{
    public const FORMAT = 'P%YY%MM%DDT%HH%IM%SS';

    #[\Override]
    public function getName(): string
    {
        return 'positive_dateinterval';
    }

    #[\Override]
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        $fieldDeclaration['length'] = 255;

        return $platform->getStringTypeDeclarationSQL($fieldDeclaration);
    }

    /** @throws ConversionException */
    #[\Override]
    public function convertToDatabaseValue(mixed $value, AbstractPlatform $platform): ?string
    {
        if ($value === null) {
            return null;
        }

        if ($value instanceof \DateInterval) {
            return $value->format(self::FORMAT);
        }

        throw ConversionException::conversionFailedInvalidType($value, $this->getName(), ['null', 'DateInterval']);
    }

    /** @throws ConversionException */
    #[\Override]
    public function convertToPHPValue(mixed $value, AbstractPlatform $platform): ?\DateInterval
    {
        if ($value === null || $value instanceof \DateInterval) {
            return $value;
        }

        $interval = $this->getDateInterval((string)$value);
        if (null === $interval) {
            throw ConversionException::conversionFailedFormat($value, $this->getName(), self::FORMAT);
        }

        return $interval;
    }

    #[\Override]
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    private function getDateInterval(string $value): ?\DateInterval
    {
        try {
            return new \DateInterval($value);
        } catch (\Throwable) {
        }

        $dateInterval = \DateInterval::createFromDateString($value);
        return ($dateInterval instanceof \DateInterval) ? $dateInterval : null;
    }
}
