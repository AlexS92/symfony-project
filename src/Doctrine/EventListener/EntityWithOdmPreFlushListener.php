<?php
declare(strict_types=1);

namespace App\Doctrine\EventListener;

use App\Doctrine\EntityWithOdmInterface;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

#[AsDoctrineListener(event: Events::preFlush)]
readonly class EntityWithOdmPreFlushListener
{
    private PropertyAccessorInterface $propertyAccessor;

    public function __construct(PropertyAccessorInterface $propertyAccessor)
    {
        $this->propertyAccessor = $propertyAccessor;
    }

    public function preFlush(PreFlushEventArgs $args): void
    {
        $identityMap = $args->getObjectManager()->getUnitOfWork()->getIdentityMap();
        /** @var array<?object> $map */
        foreach ($identityMap as $map) {
            foreach ($map as $entity) {
                if (null === $entity) {
                    continue;
                }
                if (is_a($entity, EntityWithOdmInterface::class)) {
                    $this->handle($entity);
                }
            }
        }
    }

    private function handle(EntityWithOdmInterface $entity): void
    {
        $fieldList = $entity->getOdmFieldList();
        foreach ($fieldList as $fieldName) {
            if (!$this->propertyAccessor->isReadable($entity, $fieldName)) {
                continue;
            }

            if (!$this->propertyAccessor->isWritable($entity, $fieldName)) {
                continue;
            }

            $object = $this->propertyAccessor->getValue($entity, $fieldName);
            if (null === $object) {
                continue;
            }

            $clonedObject = clone $object;
            $this->propertyAccessor->setValue($entity, $fieldName, $clonedObject);
        }
    }
}
