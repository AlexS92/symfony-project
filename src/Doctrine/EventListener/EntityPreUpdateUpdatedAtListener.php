<?php
declare(strict_types=1);

namespace App\Doctrine\EventListener;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

#[AsDoctrineListener(event: Events::preUpdate)]
readonly class EntityPreUpdateUpdatedAtListener
{
    private PropertyAccessorInterface $propertyAccessor;

    public function __construct(PropertyAccessorInterface $propertyAccessor)
    {
        $this->propertyAccessor = $propertyAccessor;
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs): void
    {
        $entity = $eventArgs->getObject();

        $isUpdateNeeded = $this->propertyAccessor->isWritable($entity, 'updatedAt') &&
            !$eventArgs->hasChangedField('updatedAt');

        if ($isUpdateNeeded) {
            $this->propertyAccessor->setValue($entity, 'updatedAt', new \DateTimeImmutable());
        }
    }
}
