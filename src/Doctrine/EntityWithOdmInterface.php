<?php
declare(strict_types=1);

namespace App\Doctrine;

/**
 * Доктрина не видит изменения во внутренних объектах которые не являются Entity.
 * Аннотация нужна для принудительного обновления полей являющихся такими объектами,
 * например ODM классов. В getOdmFieldList() - возвращаются имена полей, которые нужно
 * принудительно обновить. У этих полей должны быть сеттер(setFieldName) и геттер(getFieldName),
 * объект из этого поля клонируется и снова записывается в это поле.
 */
interface EntityWithOdmInterface
{
    /** @return string[] */
    public function getOdmFieldList(): array;
}
