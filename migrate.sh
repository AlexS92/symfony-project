#!/usr/bin/env bash

BASE_PATH=$(pwd)
MIGRATE_SQL_PATH="$BASE_PATH/migrations"

if [ -z "$CURRENT_UID" ]; then
  CURRENT_UID=$(id -u):$(id -g);
fi

docker run --rm -i \
  -v "$MIGRATE_SQL_PATH":/migrations \
  -u "$CURRENT_UID" \
  --name symfony-project-migration \
  --network application \
  migrate/migrate \
  -path=/migrations/ -database "postgres://local:local@postgres:5432/symfony?sslmode=disable" $*
