drop table if exists users cascade;
drop table if exists refresh_tokens cascade;
drop table if exists posts cascade;
drop table if exists applications cascade;

drop type if exists application_status;
drop type if exists post_status;
